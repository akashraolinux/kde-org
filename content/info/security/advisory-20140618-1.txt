KDE Project Security Advisory
=============================

Title:          KMail/KIO POP3 SSL MITM Flaw
Risk Rating:    Medium
CVE:            CVE-2014-3494
Platforms:      All
Versions:       kdelibs 4.10.95 to 4.13.2
Author:         Richard J. Moore <rich@kde.org>
Date:           17 June 2014

Overview
========

The POP3 kioslave used by kmail will accept invalid certificates without
presenting a dialog to the user due a bug that leads to an inability to
display the dialog combined with an error in the way the result is checked.

Impact
======

This flaw allows an active attacker to perform MITM attacks against the
ioslave which could result in the leakage of sensitive data such as the
authentication details and the contents of emails.

Workaround
==========

None

Solution
========

Upgrade to version 4.13.3 or apply the patch at
http://quickgit.kde.org/?p=kdelibs.git&a=commitdiff&h=bbae87dc1be3ae063796a582774bd5642cacdd5d&hp=1ccdb43ed3b32a7798eec6d39bb3c83a6e40228f

Credits
=======

Thanks to Jim Scadden for reporting this issue and writing the initial fix,
and to David Faure for reviewing and improving the fix.
