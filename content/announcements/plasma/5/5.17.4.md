---
aliases:
- ../../plasma-5.17.4
changelog: 5.17.3-5.17.4
date: 2019-12-03
description: KDE Ships Plasma 5.17.4
layout: plasma
release: plasma-5.17.4
title: KDE Plasma 5.17.4, bugfix Release for December
version: 5.17.4
---

{{< peertube "https://peertube.mastodon.host/videos/embed/5a315252-2790-42b4-8177-94680a1c78fc" >}}

{{<figure src="/announcements/plasma/5/5.17.0/plasma-5.17.png" alt="Plasma 5.17" class="text-center" width="600px" caption="KDE Plasma 5.17">}}

Tuesday, 3 December 2019.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.17.4" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in October with many feature refinements and new modules to complete the desktop experience." "5.17" >}}

This release adds three weeks' worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Discover: Fwupd, don't whine when we have unsupported hardware. <a href="https://commits.kde.org/discover/f0652fe1be1ea016a7b5734c8cab6765a1619784">Commit.</a>
- Unbreak build with Qt 5.14. <a href="https://commits.kde.org/systemsettings/32567d4f61b432ac7ed7a9e799e11041d1b1279e">Commit.</a>
- Fix Cuttlefish mouse click selection in icon grid. <a href="https://commits.kde.org/plasma-sdk/637c0e517b29c9961fdefeaa0b2b9f2317aa129a">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D25633">D25633</a>