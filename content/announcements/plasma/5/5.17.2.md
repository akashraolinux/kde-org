---
aliases:
- ../../plasma-5.17.2
changelog: 5.17.1-5.17.2
date: 2019-10-29
description: KDE Ships Plasma 5.17.2
layout: plasma
release: plasma-5.17.2
title: KDE Plasma 5.17.2, bugfix Release for October
version: 5.17.2
---

{{< peertube "https://peertube.mastodon.host/videos/embed/5a315252-2790-42b4-8177-94680a1c78fc" >}}

{{<figure src="/announcements/plasma/5/5.17.0/plasma-5.17.png" alt="Plasma 5.17" class="text-center" width="600px" caption="KDE Plasma 5.17">}}

Tuesday, 29 October 2019.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.17.2" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in October with many feature refinements and new modules to complete the desktop experience." "5.17" >}}

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- [wallpapers/image] Randomise new batches of images in the slideshow. <a href="https://commits.kde.org/plasma-workspace/9dca7d6cd44cbb16c6d7fb1aca5588760544b1d6">Commit.</a> Fixes bug <a href="https://bugs.kde.org/413463">#413463</a>. Phabricator Code review <a href="https://phabricator.kde.org/D24986">D24986</a>
- [System Settings sidebar] Add a hover effect to intro page icons. <a href="https://commits.kde.org/systemsettings/a306b76cb8531905b463e33e52b1176c0073d4f1">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D24901">D24901</a>
- Appstream: support more formats of appstream urls. <a href="https://commits.kde.org/discover/4f2aa69241717a12e09bd49aba9ff78bd202960a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/408419">#408419</a>