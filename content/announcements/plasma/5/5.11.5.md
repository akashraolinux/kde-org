---
aliases:
- ../../plasma-5.11.5
changelog: 5.11.4-5.11.5
date: 2018-01-02
description: KDE Ships 5.11.5
layout: plasma
release: plasma-5.11.5
title: KDE Plasma 5.11.5, Bugfix Release for January
version: 5.11.5
---

{{%youtube id="nMFDrBIA0PM"%}}

{{<figure src="/announcements/plasma/5/5.11.0/plasma-5.11.png" alt="KDE Plasma 5.11 " class="text-center" width="600px" caption="KDE Plasma 5.11">}}

Tuesday, 2 January 2018.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.11.5" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in October with many feature refinements and new modules to complete the desktop experience." "5.11" >}}

This release adds three weeks' worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fixed a missing icon in the appmenu applet
- Better error handling in Discover
- Improved GTK support for the Breeze theme
- Fixed a crash in the screen locker on wayland