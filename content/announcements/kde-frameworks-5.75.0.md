---
# SPDX-FileCopyrightText: 2020 David Faure <faure@kde.org>
# SPDX-FileCopyrightText: 2020 Jonathan Riddell <jr@jriddell.org>
#
# SPDX-License-Identifier: CC-BY-4.0
title: Release of KDE Frameworks 5.75.0
version: 5.75.0
qtversion: 5.12
layout: framework
date: 2020-10-10
---
 
October 10, 2020. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.75.0.
 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms. For an
introduction see the <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### Baloo

+ [AdvancedQueryParser] Relax parsing of string ending with parentheses
+ [AdvancedQueryParser] Relax parsing of string ending with comparator
+ [AdvancedQueryParser] Fix out-of-bound access if last character is a comparator
+ [Term] Replace Term::Private constructor with default values
+ [BasicIndexingJob] Shortcut XAttr retrieval for files without attributes
+ [extractor] Fix document type in extraction result
+ Relicense file to LGPL-2.0-or-later

### BluezQt

+ Add rfkill property to manager
+ Add status property to rfkill
+ Register Rfkill for QML
+ Export Rfkill
+ Support providing service data values for LE advertisements

### Breeze Icons

+ Add new generic "behavior" icon
+ Make icon validation depend on icon generation only if enabled
+ Replace 24px icon bash script with python script
+ Use flag style iconography for view-calendar-holiday
+ Add Plasma Nano logo
+ Add application-x-kmymoney
+ Add KMyMoney icon

### Extra CMake Modules

+ fix fetch-translations for invent urls
+ Include FeatureSummary and find modules
+ Introduce plausibility check for outbound license
+ Add CheckAtomic.cmake
+ Fix configuring with pthread on Android 32 bit
+ add RENAME parameter to ecm_generate_dbus_service_file
+ Fix find_library on Android with NDK &lt; 22
+ Explicitly sort Android version lists
+ Store Android {min,target,compile}Sdk in variables

### KDE Doxygen Tools

+ Licensing improvements
+ Fix api.kde.org on mobile
+ Make api.kde.org a PWA

### KArchive

+ Relicense file to LGPL-2.0-or-later

### KAuth

+ use new install var (bug 415938)
+ Mark David Edmundson as maintainer for KAuth

### KCalendarCore

+ Relicense file to LGPL-2.0-or-later

### KCMUtils

+ Remove handling for inside events from tab hack (bug 423080)

### KCompletion

+ Relicense files to LGPL-2.0-or-later

### KConfig

+ CMake: Also set SKIP_AUTOUIC on generated files
+ Use reverse order in KDesktopFile::locateLocal to iterate over generic config paths

### KConfigWidgets

+ Fix isDefault that cause the KCModule to not properly update its default state
+ [kcolorscheme]: Add isColorSetSupported to check if a colour scheme has a given color set
+ [kcolorscheme] Properly read custom Inactive colors for the backgrounds

### KContacts

+ Remove obsolete license file for LGPL-2.0-only
+ Relicense files to LGPL-2.0-or-later

### KCoreAddons

+ KJob: emit result() and finished() at most once
+ Add protected KJob::isFinished() getter
+ Deprecate KRandomSequence in favour of QRandomGenerator
+ Initialize variable in header class + const'ify variable/pointer
+ harden message-based tests against environment (bug 387006)
+ simplify qrc watching test (bug 387006)
+ refcount and delete KDirWatchPrivate instances (bug 423928)
+ Deprecate KBackup::backupFile() due to lost functionality
+ Deprecate KBackup::rcsBackupFile(...) due to no known users

### KDBusAddons

+ Relicense files to LGPL-2.0-or-later

### KDeclarative

+ QML for I18n are added in KF 5.17
+ Relicense files to LGPL-2.0-or-later
+ Block shortcuts when recording key sequences (bug 425979)
+ Add SettingHighlighter as a manual version of the highlighting done by SettingStateBinding

### KDELibs 4 Support

+ KStandardDirs: always resolve symlinks for config files

### KHolidays

+ Uncommented description fields for mt_* holiday files
+ Add national holidays for Malta in both English (en-gb) and Maltese (mt)

### KI18n

+ Relicense file to LGPL-2.0-or-later

### KIO

+ KUrlNavigator: always use "desktop:/" not "desktop:"
+ Support DuckDuckGo bang syntax in Webshortcuts (bug 374637)
+ KNewFileMenu: KIO::mostLocalUrl is useful with :local protocols only
+ Deprecate KIO::pixmapForUrl
+ kio_trash: remove unnecessarily strict permission check (bug 76380)
+ OpenUrlJob: handle all text scripts consistently (bug 425177)
+ KProcessRunner: more systemd metadata
+ KDirOperator: don't call setCurrentItem on an empty url (bug 425163)
+ KNewFileMenu: fix creating new dir with name starting with ':' (bug 425396)
+ StatJob: make it clearer that mostLocalUrl works only with :local protocols
+ Document how to add new "random" roles in kfileplacesmodel.h
+ Remove old kio_fonts hack in KCoreDirLister, hostname was stripped incorrectly
+ KUrlCompletion: accommodate ":local" protocols that use hostname in url
+ Split code of addServiceActionsTo method into smaller methods
+ [kio] BUG: Allowing double-quotes in open/save dialog (bug 185433)
+ StatJob: cancel job if url is invalid (bug 426367)
+ Connect slots explicitly instead of using auto-connections
+ Make filesharingpage API actually work

### Kirigami

+ AbstractApplicationHeader: anchors.fill instead of position-dependent anchoring
+ Improve look and consistency of GlobalDrawerActionItem
+ Remove form indent for narrow layouts
+ Revert "allow customize the header colors"
+ allow customize the header colors
+ Add missing @since for the painted area properties
+ Introduce QtQuick Image style paintedWidth/paintedHeight properties
+ Add a placeholder image property to icon (in the style of fallback)
+ Remove Icon's custom implicitWidth/implicitHeight behavior
+ Guard potentially-null pointer (turns out to be surprisingly common)
+ protected setStatus
+ Introduce status property
+ Support ImageResponse and Texture type image providers in Kirigami::Icon
+ Warn people not to use ScrollView in ScrollablePage
+ Revert "always show separator"
+ make mobilemode support custom title delegates
+ Hide breadcrumbs separator line if buttons layout is visible but 0 width (bug 426738)
+ [icon] Consider icon invalid when source is an empty URL
+ Change Units.fontMetrics to actually use FontMetrics
+ Add Kirigami.FormData.labelAlignment property
+ always show separator
+ Use Header colors for desktop style AbstractApplicationHeader
+ Use the context of the component when creating delegates for ToolBarLayout
+ Abort and delete incubators when deleting ToolBarLayoutDelegate
+ Remove actions and delegates from ToolBarLayout when they get destroyed (bug 425670)
+ Replace use of c-style pointer cast in sizegroup
+ binary constants are a C++14 extension
+ sizegroup: Fix enum not handled warnings
+ sizegroup: Fix 3arg connects
+ sizegroup: Add CONSTANT to signal
+ Fix a few cases of using range loops on non-const Qt containers
+ Constrain button height in global toolbar
+ Display a separator between breadcrumbs and the icons to the left
+ Use KDE_INSTALL_TARGETS_DEFAULT_ARGS
+ Size ApplicationHeaders using the SizeGroup
+ Introduce SizeGroup
+ Fix: make refresh indicator appear above list headers
+ put overlaysheets over drawers

### KItemModels

+ ignore sourceDataChanged on invalid indexes
+ Support for KDescendantProxyModel "collapsing" nodes

### KNewStuff

+ Manually track the life cycle of our kpackage runner internals
+ Update versions for "fake" kpackage updates (bug 427201)
+ Do not use default parameter when not needed
+ Fix crash when installing kpackages (bug 426732)
+ Detect when the cache changes and react accordingly
+ Fix updating of entry if version number is empty (bug 417510)
+ Const'ify pointer + initialize variable in header
+ Relicense file to LGPL-2.0-or-later
+ Accept suggest takeover of maintainership

### KNotification

+ Lower Android plugin until new Gradle is available
+ Use Android SDK versions from ECM
+ Deprecate KNotification constructor taking widget parameter

### KPackage Framework

+ Fix DBus notifications when installed/updated
+ Relicense file to LGPL-2.0-or-later

### KParts

+ Install krop &amp; krwp servicetype definition files by file name matching type

### KQuickCharts

+ Avoid binding loop inside Legend
+ Remove check for GLES3 in SDFShader (bug 426458)

### KRunner

+ Add matchRegex property to prevent unnecessary thread spawning
+ Allow to set actions in QueryMatch
+ Allow to specify individual actions for D-Bus runner matches
+ Relicense files to LGPL-2.0-or-later
+ Add min letter count property
+ Consider XDG_DATA_HOME env variable for template install dirs
+ Improve error messages for D-Bus runners
+ Start to emit metadata porting warnings at runtime

### KService

+ bring back disableAutoRebuild from the brink (bug 423931)

### KTextEditor

+ [kateprinter] Portaway from deprecated QPrinter methods
+ Don't create temporary buffer to detect mimetype for saved local file
+ avoid hang due to dictionary and trigrams loading on first typing
+ [Vimode]Always show  a-z buffers in lower case
+ [KateFadeEffect]emit hideAnimationFinished() when a fade out is interrupted by a fade in
+ ensure pixel perfect border even for scaled rendering
+ ensure we overpaint the border separator over all other stuff like folding highlights
+ move separator from between icon border and line numbers to between bar and text
+ [Vimode]Fix behavior of numbered registers
+ [Vimode]Put deleted text to the proper register
+ Restore behavior of find selected when no selection is available
+ no further LGPL-2.1-only or LGPL-3.0-only files
+ re-license files to LGPL-2.0-or-later
+ use not needed set method for some theme colors
+ 5.75 will be once incompatible, default to 'Auto Color Theme Selection' for themes
+ alter scheme =&gt; theme in the code to avoid confusion
+ shorten proposed license header to current state
+ ensure we always end up with some valid theme
+ improve Copy... dialog
+ fix more new =&gt; copy naming
+ add some KMessageWidget that hints for read-only themes to copy them, rename new =&gt; copy
+ disable editing of read-only themes
+ saving of highlighting specific style overrides works, only diffs are saved
+ simplify attribute creation, transparent colors are now properly handled in Format
+ try to limit export to changes attributes, this halfway works, but we still export the wrong names for included definitions
+ start to compute the 'real' defaults based on current theme and formats without style overrides for the highlighting
+ fix reset action
+ storing of syntax specific overrides, at the moment just all stuff that got loaded into the tree view is stored
+ start to work on syntax highlighting specific overrides, at the moment, show just the styles a highlighting really has itself
+ allow default style changes changes to be saved
+ allow color changes to be saved
+ implement theme export: simple file copy
+ no highlighting specific import/export, makes no sense with new .theme format
+ implement .theme file import
+ theme new &amp; delete work, new will copy the current theme as start point
+ use theme colors everywhere
+ rip out more old schema code in favor of KSyntaxHighlighting::Theme
+ start to use the colors as set by the theme, without own logic around this
+ initialize m_pasteSelection and increase UI file version
+ add shortcut for paste mouse selection
+ avoid setTheme, we just can pass our theme to the helper functions
+ fix tooltip, this will just reset to theme default
+ export default styles configuration to json theme
+ start to work on theme json export, activated by using the .theme extension in the export dialog
+ rename 'Use KDE Color Theme' to 'Use Default Colors', that is the actual effect
+ don't ship empty 'KDE' theme by default
+ support automatic selection of right theme for current Qt/KDE color theme
+ convert old theme names to new ones, use new config file, transfer data once
+ move font config to appearance, rename scheme =&gt; color theme
+ remove hardcoded default theme name, use KSyntaxHighlighting accessors
+ load fallback colors from theme
+ don't bundle embedded colors at all
+ use right function to lookup theme
+ editor colors are now used from Theme
+ use the KSyntaxHighlighting::Theme::EditorColorRole enum
+ handle Normal =&gt; Default transition
+ first step: load theme list from KSyntaxHighlighting, now we have them already as known schemes in KTextEditor

### KUnitConversion

+ Use uppercase for Fuel Efficiency

### KWayland

+ Don't cache QList::end() iterator if erase() is used

### KWidgetsAddons

+ kviewstateserializer.cpp - crash guards in restoreScrollBarState()

### KWindowSystem

+ Relicense files to be compatible with LGPL-2.1

### KXMLGUI

+ [kmainwindow] Don't delete entries from an invalid kconfiggroup (bug 427236)
+ Don't overlap main windows when opening additional instances (bug 426725)
+ [kmainwindow] Don't create native windows for non-toplevel windows (bug 424024)
+ KAboutApplicationDialog: avoid empty "Libraries" tab if HideKdeVersion is set
+ Show language code in addition to (translated) language name in switch application language dialog
+ Deprecate KShortcutsEditor::undoChanges() in favour of new undo()
+ Handle double close in main window (bug 416728)

### Plasma Framework

+ Fix plasmoidheading.svgz being installed to the wrong place (bug 426537)
+ Provide a lastModified value in ThemeTest
+ Detect that we are looking for an empty element and quit early
+ Make PlasmaComponents3 Tooltips use the typical tooltip style (bug 424506)
+ Use Header colors in PlasmoidHeading headers
+ Change PC2 TabBar highlight movement animation easing type to OutCubic
+ Add support for Tooltip color set
+ Fix PC3 Button/ToolButton icons not always having the right color set (bug 426556)
+ Ensure FrameSvg uses lastModified timestamp when using cache (bug 426674)
+ Ensure we always have a valid lastModified timestamp when setImagePath is called
+ Deprecate a lastModified timestamp of 0 in Theme::findInCache (bug 426674)
+ Adapt QQC2 import to new versioning scheme
+ [windowthumbnail] Verify that the relevant GLContext exists, not any
+ Add missing PlasmaCore import to ButtonRow.qml
+ Fix a few more reference errors in PlasmaExtras.ListItem
+ Fix error for implicitBackgroundWidth in PlasmaExtras.ListItem
+ Call edit mode "Edit Mode"
+ Fix a TypeError in QueryDialog.qml
+ Fix ReferenceError to PlasmaCore in Button.qml

### QQC2StyleBridge

+ Also use highlight text color when checkDelegate is highlighted (bug 427022)
+ Respect scrollbar click to jump to position setting (bug 412685)
+ Don't inherit colors in desktop toolbar style by default
+ Relicense file to LGPL-2.0-or-later
+ Use header colors only for header toolbars
+ Move color set declaration to a place where it can be overridden
+ Use Header colors for Desktop style ToolBar
+ add the missing isItem property necessary for trees

### Sonnet

+ Downgrade trigrams output

### Syntax Highlighting

+ AppArmor: fix regexp of paths detection
+ AppArmor: update highlighting for AppArmor 3.0
+ color cache for rgb to ansi256colors conversions (speeds up markdown loading)
+ SELinux: use include keywords
+ SubRip Subtitles &amp; Logcat: small improvements
+ generator for doxygenlua.xml
+ Fix doxygen latex formulas (bug 426466)
+ use Q_ASSERT like in remaining framework + fix asserts
+ rename --format-trace to --syntax-trace
+ apply a style to regions
+ trace contexts and regions
+ use editor background color by default
+ ANSI highlighter
+ Add copyright and update separator color in Radical theme
+ Update separator color in the Solarized themes
+ Improve color of separator and icon border for Ayu, Nord and Vim Dark themes
+ make separator color less intrusive
+ import Kate schema to theme converter by Juraj Oravec
+ more prominent section about licensing, link our MIT.txt copy
+ first template for base16 generator, https://github.com/chriskempson/base16
+ add proper license information to all themes
+ Add Radical color theme
+ Add Nord color theme
+ improve themes showcase to show more styles
+ adding gruvbox light and dark themes, MIT licensed
+ Add ayu color theme (with light, dark and mirage variants)
+ Add POSIX alternate for simple variable assignment
+ tools to generate a graph from a syntax file
+ fix auto-conversion of unset QRgb == 0 color to "black" instead of "transparent"
+ Add Debian changelog and control example files
+ add the 'Dracula' color theme

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

