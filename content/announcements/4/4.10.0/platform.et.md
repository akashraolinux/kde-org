---
title: KDE platvorm 4.10 viib rohkem API-sid üle Qt Quickile
date: "2013-02-06"
hidden: true
---

## Plasma SDK

KDE arendusplatvormi käesolevas väljalaskes leiab senisest märksa täielikuma Plasma SDK. Varasemad eraldi komponendid, näiteks plasmoidviewer, plasmaengineexplorer ja plasmawallpaperviewer, kuuluvad nüüd kõik Plasma vidinate arendamise tööriistakomplekti <a href="http://techbase.kde.org/Projects/Plasma/PlasMate">Plasmate</a> koosseisu.

<div class="text-center">
	<a href="/announcements/4/4.10.0/plasmate.png">
	<img src="/announcements/4/4.10.0/thumbs/plasmate.png" class="img-fluid">
	</a><br/>
  <em>
  Plasmate kujutab endast Plasma SDK südant
 </em>
</div>
<br/>

## Qt Quicki toetus

<a href="http://doc.qt.digia.com/qt/qtquick.html">Qt Quicki</a> kasutamine Plasmas aina areneb. Paljud koostisosad on uuendatud, et need pruugiksid kasutajaliideses ainult Qt Quicki, mis ühtlasi muudab hõlpsaks Plasma töötsoonide laiendamise ja kohandamise. Plasma võimaldab nüüd kirjutada ka konteinereid (mille ülesanne on esitada vidinaid töölaual ja paneelidel) ainult Qt Quicki hõlpsasti omandatavat arenduskeelt kasutades. See lubab arendajatel pakkuda katsetamiseks või erijuhtudeks kohandatud konteinereid. Sel moel on Plasma muutunud väärtuslikuks ja universaalseks kasutajaliidese tööriistakomplektiks.

## Töölauaefektid skriptidega

Töölaua efektide, käitumise ja haldamise skriptimisliidesd muudavad KWini aknahalduse arendajate silmis tulusaks tööriistaks, mille abil lahendada konkreetseid erijuhtumeid. Lisaks vähendab modulaarne lähenemine tublisti KWini põhiosa suurust. Paraneb ka hooldatavus, kui spetsiaalseteks juhtumiteks mõeldud kood jätta väliste skriptide hooleks. Võrreldes C++ koodiga, mida skriptid asendavad, on koodi kirjutamine, hooldamine ja kvaliteedi tagamine palju kergem.

<h4>KDE arendusplatvormi paigaldamine</h4>

KDE tarkvara, sealhulgas kõik teegid ja rakendused, on vabalt saadaval vastavalt avatud lähtekoodiga tarkvara litsentsidele. KDE tarkvara töötab väga mitmesugusel riistvaral ja protsessoritel (näiteks ARM ja x86), operatsioonisüsteemides ning igasuguste aknahaldurite ja töökeskkondadega. Lisaks Linuxile ja teistele UNIX-il põhinevatele süsteemidele leiab enamiku KDE rakenduste Microsoft Windowsi versioonid leheküljelt <a href="http://windows.kde.org">KDE software on Windows</a> ja Apple Mac OS X versioonid leheküljelt <a href="http://mac.kde.org/">KDE software on Mac</a>. Veebist võib leida KDE rakenduste eksperimentaalseid versioone mitmele mobiilsele platvormile, näiteks MeeGo, MS Windows Mobile ja Symbian, kuid need on esialgu ametliku toetuseta. <a href="http://plasma-active.org">Plasma Active</a> kujutab endast kasutajakogemust paljudele seadmetele, näiteks tahvelarvutid ja muud mobiilsed seadmed.
<br />
KDE tarkvara saab hankida lähtekoodina või mitmesugustes binaarvormingutes aadressilt <a href="http://download.kde.org/stable/4.10.0/">http://download.kde.org</a>, samuti
<a href="http://www.kde.org/download/cdrom">CD-ROM-il</a>
või ka mis tahes tänapäevasest <a href="http://www.kde.org/download/distributions">
GNU/Linuxi ja UNIX-i süsteemist</a>.

<a id="packages"><em>Packages</em></a>.
Mõned Linux/UNIX OS-i tootjad on lahkelt valmistanud 4.10.0
binaarpaketid mõnele oma distributsiooni versioonile, mõnel juhul on sama teinud
kogukonna vabatahtlikud. <br />
<a id="package_locations"><em>Pakettide asukohad</em></a>.
Praegu saadaolevate binaarpakettide nimekirja, millest KDE väljalaskemeeskond on teadlik,
näeb vastaval <a href="/info/4.10.0">4.10 infoleheküljel</a>.

<a id="source_code"></a>
Täieliku 4.10.0lähtekoodi võib vabalt alla laadida <a href="http://download.kde.org/stable/4.10.0/src/">siit</a>.
Juhiseid KDE tarkvarakomplekti 4.10.0kompileerimiseks ja paigaldamiseks
leiab samuti <a href="/info/4.10.0#binary">4.10.0infoleheküljelt.

## Täna ilmusid veel:

<h2> <a href="../plasma"><img src="/announcements/4/4.10.0/images/plasma.png" class="app-icon float-left mr-3" alt="The KDE Plasma Workspaces 4.10" />  Plasma töötsoonid 4.10 parandavad mobiilsete seadmete toetust ja näevad kaunimad välja
</a></h2>

Mitmed Plasma töötsoonide komponendid porditi Qt Quick/QML-i raamistikule. Stabiilsus ja kasutatavus on paranenud. Lisandunud on uus trükkimishaldur ja värvihalduse toetus.

<h2> <a href="../applications"><img src="/announcements/4/4.10.0/images/applications.png" class="app-icon float-left mr-3" alt="The KDE Applications 4.10"/>
KDE rakendused: hõlpsamad kasutada, parema jõudlusega ja viivad lausa Marsile
</a></h2>

KDE rakendustes on eriti tuntavaid täiendusi saanud Kate, KMail ja Konsool. KDE õpirakendustes on põhjalikult muudetud KTouchi, aga muutusi on teisigi. KDE mängud pakuvad uut mängu Picmi ning mitmeid mängimist parandamist täiustusi.

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
