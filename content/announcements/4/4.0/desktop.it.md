---
title: "The KDE 4.0 Desktop"
hidden: true
---

<h2>Plasma</h2>

<div class="text-center">
<a href="/announcements/4/4.0/desktop.png">
<img src="/announcements/4/4.0/desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>Il Desktop di KDE 4.0</em>
</div>
<br/>

<p>
Plasma è il nuovo gestore del desktop di KDE. Fornisce nuovi strumenti per avviare applicazioni, rappresenta la nuova interfaccia di KDE e offre nuove vie per interagire con il tuo desktop.
</p>
<p>
La nuova funzione dashboard di Plasma rimpiazza la vecchia «Mostra desktop». Abilitando la dashboard si nascondono tutte le finestre e i plasmoidi vengono sovrapposti ad esse. Premi CTRL+F12 per vedere l'effetto dashboard in azione, potrai così vedere i tuoi plasmoidi, leggere le notizie, controllare le condizioni atmosferiche... Tutto questo può essere fatto con Plasma.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/dashboard.png">
<img src="/announcements/4/4.0/dashboard_thumb.png" class="img-fluid">
</a> <br/>
<em>La dashboard di Plasma</em>
</div>
<br/>

<h3>Avvia le applicazioni, cerca e apri pagine web con KRunner</h3>
<p>
<strong>KRunner</strong> ti permette di avviare velocemente le applicazioni. Premi ALT+F2 per vedere la finestra di dialogo di KRunner. Mentre stai digitando qualcosa KRunner ti mostrerà tutto ciò che corrisponde a quello che stai digitando:

<div class="text-center">
<a href="/announcements/4/4.0/krunner-desktop.png">
<img src="/announcements/4/4.0/krunner-desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>Avvia le applicazioni con KRunner</em>
</div>
<br/>

<ul>
  <li>
    Digita il nome di una applicazione, KRunner troverà le applicazioni che corrispondono alla tua richiesta. Scegli l'applicazione che ti interessa oppure premi direttamente invio se quello che cercavi è al primo posto della lista. Per dare uno sguardo alle applicazioni che sono attive nel tuo sistema prova a fare click su «Mostra attività di sistema», oppure premi ALT+S. Per una rappresentazione grafica avvia invece KSysguard.
  </li>
  <li>
Krunner funziona anche come una piccola calcolatrice. Prova ad inserire una euqazione del tipo "=13.37*2", e KRunner mostrerà istantaneamente i risultati.

<div class="text-center">
<a href="/announcements/4/4.0/krunner-calculator.png">
<img src="/announcements/4/4.0/krunner-calculator.png" class="img-fluid">
</a> <br/>
<em>KRunner usato come una calcolatrice</em>
</div>
<br/>

  </li>
  <li>
    Apri i tuoi segnalibri nel browser direttamente da KRunner tramite le scorciatoie! Digita "gg:kde4 visual guide" per cercare con Google «kde4 visul guide». 
Digita "wp:kde" per raggiungere la pagina su KDE di Wikipedia. Torverai che un sacco di scorciatoie preconfigurate inaugurano una nuova e facile maniera di interagire con il web direttamente dal desktop. Una lista dettagliata delle scorciatoie raggiungibili da KRunner (e alcune anche da Konqueror, il browser di KDE) può essere trovata nella finestra delle configurazioni di Konquero dal menu «Impostazioni». "Configura Konqueror" apre la finestra delle configurazioni, «Scorciatoie del web» può essere trovato nell'elenco delle impostazioni sulla sinistra.
  </li>
</ul>
</p>

<h3>KickOff</h3>
<p>
KickOff è un nuovo menu di KDE.
Fai click sul logo di KDE posizionato nell'angolo in basso a sinistra del tuo schermo. KickOff si avvierà dandoti facile accesso alle applicazioni installate, alla cronoligia dei file e delle applicazioni utilizzati di recente e molto altro. Talla scheda «Esci» potrai lasciare la sessione, riavviare o spegnare il computer.

<div class="text-center">
<a href="/announcements/4/4.0/kickoff-favorites.png">
<img src="/announcements/4/4.0/kickoff-favorites_thumb.png" class="img-fluid">
</a> <br/>
<em>facile accesso alle applicazioni con KickOff</em>
</div>
<br/>

<ul>
  <li>
    I <strong>Preferiti</strong>. Questa scheda mostra un insieme di applicazioni o documenti ai quali accedi spesso, infatti è visualizzata per prima. Puoi facilmente personalizzare questo insieme facendo click destro sull'elemento del menu che vuoi aggiungere e scegliendo «aggiungi ai preferiti» oppure «rumuovi dai preferiti».
  </li>
  <li>
    La scheda <strong>Applicazioni</strong> mostra una lista di programmi suddivisa per categorie. Muovendo il mouse fra le schede alla base di KickOff, e in un batter d'occhio si passerà alla scheda delle applicazioni. Naviga attraverso i menu per trovare l'applicazione che cerchi. Mentre navighi tra le applicazioni puoi tornare sui tuoi passi facendo click con il pulsante sinistro sul largo bottone laterale. Un suggerimento: Per accedere a KickOff guida il puntatore del mouse nell'angolo in basso a sinistra, questo ti eviterà di farci click ssopra.
  </li>
  <li>
	La scheda <strong>Computer</strong> fornisce l'accesso ai dispositivi come dischi rigidi, memorie flash USB e altri dispositivi rimovibili. C'è anche un collegamento alle cartelle preferite e alle impostazioni di sistema.
  </li>
  <li>
	la scheda della <strong>Cronologia</strong> mostra le applicazioni e i documenti che sono stati utilizzati di recente. Da quì è facile accedere ai documenti ai quali si è lavorato di recente.
  </li>
  <li>
	La scheda <strong>Esci</strong> offre le opzioni di spegnimento, riavvio e uscita dalla sessione. Se hai intenzione di ibernare il computer premi il pulsante «Spegni», dopodiche, nel menù di fine sessione tieni premuto su spegni. Compariranno a questo punto le opzioni per ibernare e sospendere il computer, se il sistema lo permette.
  </li>
</ul>
</p>

<h3>Il pannello</h3>

<div class="text-center">
<a href="/announcements/4/4.0/panel.png">
<img src="/announcements/4/4.0/panel_thumb.png" class="img-fluid">
</a> <br/>
<em>Il pannello ospita KickOff, la barra delle applicazioni, l'anteprima e gestione dei desktop, l'orologio, il vassoio di sistema e altro</em>
</div>
<br/>

<p>
Se cerchi qualcosa di specifico, fai click sull'icona di KDE in basso a sinistra per avere accesso a KickOff e digita quello che stai cercando. KickOff cercherà di trovare le applicazioni che corrispondono ai criteri di ricerca. Digitando ad esempio «cd» verranno visualizzate le applicazioni per ascoltare, masterizzare o codificare un CD o un DVD. Alla stessa maniera, digitando «visualizzatore» verranno mostrate le applicazioni utili a leggere ogni sorta di documenti.
</p>
<p>
Il <strong>Pannello</strong> di Plasma ospita il menu, il vassoio di sistema, e la barra delle applicazioni. Quest'ultima può visualizzare anteprime in tempo reale delle finestre che sono al momento nascoste.
<br />
</p>
<p>
Sul pannello possiamo trovare anche l'anteprima e gestione dei desktop. Utilizza questo plasmoide per gestire le aree di lavoro, anche dette «Desktop virtuali». Facendoci click con il pulsante destro avrai accesso alla configurazione del numero e della disposizione dei desktop. Se hai gli effetti desktop abilitati premi CTRL+F8 per una anteprima a schermo intero delle aree di lavoro.
</p>

<p>
Se vuoi saperne di più su Plasma dai un'occhiata alle <a href="http://techbase.kde.org/Projects/Plasma/FAQ">Plasma FAQ</a> su TechBase.
</p>

<h2>KWin - Il window manager di KDE</h2>
<p>
Kwin è il consolidato, stabile gestore delle finestre di KDE, è stato migliorato per poter utilizzare le moderne tecnologie di accelerazione grafica per poter garantire una interazione migliorata con le finestre. Fai click con il pulsante destro sul titolo di una finestra e selezione «Configurazione della finestra...» per andare alle configurazioni avanzate di Kwin. Quì potrai gestire gli effetti grafici e le altre funzionalità avanzate di Kwin. Per esempio nelle «Azioni finestra» puoi configurare Kwin per massimizzare la finestra quando vinene fatto doppio click sulla barra del titolo.
</p>
<p>
Suggerimento: Kwin ti permette di spostare facilmente le finestre tenendo premuto ALT. Facendolo, puoi semplicemente tenere premuto il tasto destro del mouse e muovere la finestra. Utilizzando invece il tasto destro la finestra sarà ridimensionata. Non ti basta che tenere premuto ALT e fare delle tue finestre quello che vuoi!
</p>

<h3>Effetti Desktop</h3>
<p>
Abilita gli effetti Grafici. Kwin ti da la possibilità di interagire con le finestre in modi nuovi. Fai click destro sulla barra del titolo di una finestra e scegli l'opzione «Configurazione della finestra...», vai nella sezione «Effetti Desktop» e scegli «Abilita effetti grafici». Accetta i cambiamenti e premi «OK» o più semplicemente «Applica». Ora gli effetti grafici di Kwin sono abilitati!
</p>

<div class="text-center">
<a href="/announcements/4/4.0/kwin-presentwindows.png">
<img src="/announcements/4/4.0/kwin-presentwindows_thumb.png" class="img-fluid">
</a> <br/>
<em>Passa da una applicazione all'altra con i nuovi effetti di Kwin</em>
</div>
<br/>

<p>
    <strong>Presenta le finestre</strong> è un effetto che mostra le miniature di tutte le finestre aperte sul desktop. Porta il cursore del mouse nell'angolo in alto a sinistra dello schermo, le finestre verranno mostrate ridotte permettendoti di vederle tutte. Ora puoi facilmente selezionarne una facendoci click sopra. Puoi anche digitare delle lettere per filtrare le finestre visualizzate in base al loro nome o al loro titolo. Puoi attivare l'effettp «Presenta le finestre» anche dala tastiera con CONTROL+F9, usa CONTROL+F10 per vedere le finestre di tutti i desktop.
</p>
<p>
Suggerimento: Con l'effetto «Presenta le finestre» attivo e in funzione digita il nome di una applicazione o il titolo di una finestra. Le finestre verranno così filtrate in base a quello che scrivi sulla tastiera e rimarranno visualizzate solo quelle che corrispondono a quello che digiti. Ciò facilita senza dubbio la ricerca della finestra giusta.
</p>
<p>
<strong>Griglia dei Desktop</strong> è un effetto che allontana la visuale dal desktop attivo e mostra il contenuto di tutti gli spazi di lavoro. In questa visualizzazioe puoi spostare le finestre da uno spazio di lavoro ad un altro e cliccando su uno di essi si torna alla normale visualizzazione sul desktop virtuale selezionato.
</p>
<p>
Suggerimento: Puoi anche digitare il numero del desktop che vuoi raggiungere. Abilita l'effetto con <em>CONTROL+F8</em> e digita un numero che corrisponda ad un desktop. <br />
Fai click con la rotellina del mouse su di una finestra per renderla disponibile su tutte le aree di lavoro.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/desktopgrid.png">
<img src="/announcements/4/4.0/desktopgrid_thumb.png" class="img-fluid">
</a> <br/>
<em>L'effetto Griglia dei Desktop offre le stesse funzionalità del Pager</em>
</div>
<br/>

<p> 
Il Pannello ospita un applet che fornisce funzionalità simili a quelle dell'effetto «Griglia dei Desktop», am che è disponibile anche quando gli effetti grafici sono disabilitati. Fai click conil pulsante destro sul «Pager» nel pannello e configura il numero dei desktop virtuali e altre opzioni. Puoi trascinare il plasmoide del «Pager» dalla finestra di selezione dei Plamoidi sia sul desktop che sul pannello. Gli screenshots mostrano queste possibilità.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/pager.png">
<img src="/announcements/4/4.0/pager.png" class="img-fluid">
</a> <br/>
<em>Il Pager permette di muoversi attraverso gli spazi di lavoro</em>
</div>
<br/>

<p>
L'effetto <strong>Anteprime per la barra delle applicazioni</strong> abilita delle anteprime in tempo reale delle finestre che possono essere visualizzate passando sopra i nomi delle applicazioni nella barra delle applicazioni sul Pannello. Questo rende più facile mantenere sotto controllo lo stato delle applicazioni minimizzate.  Anteprime per la barra delle applicazioni è anche d'aiuto quando ci si deve spostare da un'applicazione ad un'altra.

<div class="text-center">
<a href="/announcements/4/4.0/kwin-taskbarthumbnails.png">
<img src="/announcements/4/4.0/kwin-taskbarthumbnails_thumb.png" class="img-fluid">
</a> <br/>
<em>Passaggio tra le applicazioni più facile con le anteprime in tempo reale</em>
</div>
<br/>

</p>
<p>
Gli effetti desktop, o più tecnicamente gli effetti di composizione, di KWin abilitano le  <strong>trasparenze</strong> in molte applicazioni. Per esempio, Konsole l'emulatore di terminale di KDE può far uso di uno sfondo trasparente per far si che le applicazioni che stanno sotto possano essere viste. Oltretutto è anche un bell'effetto che rende il lavoro più piacevole! Puoi anche cambiare l'intesità della trasparenza di una finestra facendo click destro sul titolo e scegliendo «Cambia livello opacità».

<div class="text-center">
<a href="/announcements/4/4.0/kwin-transparency.png">
<img src="/announcements/4/4.0/kwin-transparency_thumb.png" class="img-fluid">
</a> <br/>
<em>Change the opacity of individual windows and applications</em>
</div>
<br/>

</p>
<p>
Nella finestra di configurazione degli effetti si possono controllare varie opzioni. Spesso effetti grafici sono configurabili e possono essere adattati alle esigenze personali. KWin cerca di abilitare gli effetti automaticamente in base alle prestazioni della scheda grafica perciò è probabile che gli effetti siano configurati automaticamente nel vostro sistema.<br /><br />
Se vuoi approfondire l'argomento, soprattutto per quanro riguarda le capacità di composizione di KWin fate riferimento alle <a href="http://websvn.kde.org/*checkout*/trunk/KDE/kdebase/workspace/kwin/NOTES_4_0">Note di rilascio di KWin/a> oppure approfondite su <a href="http://techbase.kde.org/Projects/KWin">TechBase</a>.
</p>

<h2>Sweet Spots</h2>
<p>
KDE fa un uso intenso di «Sweet spots» sullo schermo. Angoli e bordi, che sono facilmente raggiungibili, rendono in questo modo i pulsanti sulle decorazioni delle finestre più accessibili. Quando hai una finestra massimizzata raggiungi l'angolo in alto a destra dello schermo e fai click. La finestra verrà chiusa. Il pulsante di chiusura però è configurato in maniera tale da non creare conflitti con gli altri pulsanti così, per esempio, massimizzando una finestra non correrai il rischio di chiuderla accidentalmente. La disposizione dei pulsanti può essere inoltre cambiata a piacere per adeguarsi al tuo stile di lavoro. Fail click destro sul titolo di una finestra e scegli «Configurazione della finestra...»
<br />
Suggerimento: Per muovere una barra di scorrimento la via più facile sarà raggiungere il bordo dello schermo e tenere premuto il pulsante sinistro del mouse.
</p>

<table width="100%">
	<tr>
		<td width="50%">
      <a href="../guide">
      <img src="/announcements/4/4.0/images/star-32.png" />
				Indice
				</a>		
		</td>
		<td align="right" width="50%">
				<a href="../applications">Nella prossima pagina: Applicazioni
        <img src="/announcements/4/4.0/images/applications-32.png" /></a>		</td>
	</tr>
</table>
