---
aliases:
- /announcements/plasma-5.11.3-5.11.4-changelog
hidden: true
plasma: true
title: Plasma 5.11.4 Complete Changelog
type: fulllog
version: 5.11.4
---

### <a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a>

- ReceiveFileJob: Emit correct target path in description signal. <a href='https://commits.kde.org/bluedevil/cd65c59f6ac0d16d76d1b3f8478454c7a9ac4a5b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384740'>#384740</a>

### <a name='breeze-plymouth' href='https://commits.kde.org/breeze-plymouth'>Breeze Plymouth</a>

- Fix overlapping labels on systems with different window sizes. <a href='https://commits.kde.org/breeze-plymouth/cc1019e2938a9ac9bf9a6e7149b55deec90e010a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8721'>D8721</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- PK: Use the stream to store the information rather than the transaction. <a href='https://commits.kde.org/discover/74b1746182a271112441b0493d3b63b7456c6d87'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386992'>#386992</a>
- Flatpak: Make sure we don't degrade resources' state. <a href='https://commits.kde.org/discover/47c19354886e09fc9977e47eec1d6ae71cc2b565'>Commit.</a>
- If it's busy, try again in a second. <a href='https://commits.kde.org/discover/0c558176124cae3e3ca77f19e2000b713c0615e9'>Commit.</a>
- Group updater refreshes. <a href='https://commits.kde.org/discover/0cd350842af3ce9b7b7187d5a7cb493223527170'>Commit.</a>
- StandardUpdater: Only refresh when it's going to make a difference. <a href='https://commits.kde.org/discover/2d304b655cbf340e132f85f37bc607d9893da513'>Commit.</a>
- Narrow down the scope of a variable. <a href='https://commits.kde.org/discover/0eeb9866510a248557f3db2131174193a7a2d489'>Commit.</a>
- Make it possible for updaters to show notifications at any time. <a href='https://commits.kde.org/discover/d91eb6d359a177e732051104b2500879023a7599'>Commit.</a> See bug <a href='https://bugs.kde.org/387211'>#387211</a>
- Remove unused code. <a href='https://commits.kde.org/discover/a0a7aea36e845c0effc044dd7cc6bb2e37fe9abf'>Commit.</a>
- Remove hidden styling from the delegate's comment. <a href='https://commits.kde.org/discover/df1f776d7574d45482bc9e8490f7aeac72291c30'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387221'>#387221</a>
- No need to explicitly notify. <a href='https://commits.kde.org/discover/997c1a0bdd6b768257dad076ae1a3cdf336ba51c'>Commit.</a>
- Make sure we remove the Transaction before it's destroyed. <a href='https://commits.kde.org/discover/52ad9fc8bfa3f3ccb081aa427eaad4134d4b8157'>Commit.</a>
- Revert "Install related ref instead attempting to upgrade it". <a href='https://commits.kde.org/discover/7fd628bc988fbf15106d6546516c33bd8e483350'>Commit.</a>
- Install related ref instead attempting to upgrade it. <a href='https://commits.kde.org/discover/c0622af525da267e3ea1245da66e10a00f526a1c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8784'>D8784</a>
- Link to a url that still exists. <a href='https://commits.kde.org/discover/bf3812d340ef80ae24a43fcbbb81693de21b30f9'>Commit.</a>
- Don't fetch updates while the updater is working. <a href='https://commits.kde.org/discover/75be774910ab633bc6706057cf99f3fcef53bb08'>Commit.</a>
- Remove weirdly long timer to display notifications. <a href='https://commits.kde.org/discover/8576e93351646c02325cda94ad1867ae0af9605c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381812'>#381812</a>
- Simplify notifier logic. <a href='https://commits.kde.org/discover/32ced8ea8a0a095b9017975027937902f652e9f8'>Commit.</a>
- Don't crash if we get into a weird state. <a href='https://commits.kde.org/discover/63fbc9bf5ef1cbf7fa4743770f032fbe342aa53f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385637'>#385637</a>
- Remove unneeded declaration. <a href='https://commits.kde.org/discover/be4e1e4cc72813aafa2d776ececd1aab16e7e440'>Commit.</a>
- Make sure we never search on an invalid KNS backend. <a href='https://commits.kde.org/discover/11d121cc827198c687e9cce4603a1293c158342d'>Commit.</a> See bug <a href='https://bugs.kde.org/386045'>#386045</a>

### <a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a>

- Add missing dependencies. <a href='https://commits.kde.org/kactivitymanagerd/3fc863da3cf1da392c4253ca7f90850330438602'>Commit.</a>

### <a name='khotkeys' href='https://commits.kde.org/khotkeys'>KDE Hotkeys</a>

- Add missing include. <a href='https://commits.kde.org/khotkeys/231132690004e1dc4dfc6bac248008c50032be7c'>Commit.</a>

### <a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a>

- Use OCS to retrieve Tabs from the KDE store. <a href='https://commits.kde.org/ksysguard/fdead6c30886e17ea0e590df2e7ddb79652fb328'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/338669'>#338669</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8734'>D8734</a>

### <a name='kwayland-integration' href='https://commits.kde.org/kwayland-integration'>KWayland-integration</a>

- Add missing dependencies. <a href='https://commits.kde.org/kwayland-integration/f3ffb932d2b9832b9df464b443186fe645ec4ac8'>Commit.</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Allow glXChooseFBConfig to return sRGB capable fbconfig. <a href='https://commits.kde.org/kwin/9300aa82be77ee23c346b85fb49091ab9728aba0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387159'>#387159</a>
- Fix leaking of FDs requested from logind. <a href='https://commits.kde.org/kwin/96af5965eeeff089b1116c8ac8da6b9bd4f2a4b5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8887'>D8887</a>
- Fix race condition with libinput events on startup. <a href='https://commits.kde.org/kwin/d7d78e2b59374a43af7941e3babe904071589274'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8888'>D8888</a>
- Fix build with a KWindowSystem framework that doesn't pull QWidget. <a href='https://commits.kde.org/kwin/db6c7e17e6151b29f1c1fa7fff5a27e9ec8c584a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8706'>D8706</a>
- Allow a cross-process check for same applications. <a href='https://commits.kde.org/kwin/1ae7990a959ce2c3fad0a6aef684cf058b07cf1e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386043'>#386043</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8661'>D8661</a>
- Don't use XDGv6 in stable. <a href='https://commits.kde.org/kwin/68e4deb4902b76ae32e461011ae5f58dcca9f145'>Commit.</a>
- Support modifier+mouse button on window decoration. <a href='https://commits.kde.org/kwin/5313b856468015c732f28dfb275e98fe07345b3e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386708'>#386708</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8758'>D8758</a>
- Remove unnecessary QString::arg call. <a href='https://commits.kde.org/kwin/73f5b09e3bbe62b0a09996d244eec5328f758dcc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8764'>D8764</a>
- Fix occassional crash caused by needlessly delayed signals (bko#363224). <a href='https://commits.kde.org/kwin/55f169d1c34491f0d97c8b7a162f4ba60f6849e2'>Commit.</a> See bug <a href='https://bugs.kde.org/363224'>#363224</a>

### <a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a>

- XRandR: Clear EDID data when monitor is disconnected from an output. <a href='https://commits.kde.org/libkscreen/a9683661f856f374de2addf018d9d1ce69c8dbe3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8840'>D8840</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Fix crash in KAStatsFavouritesModel. <a href='https://commits.kde.org/plasma-desktop/6e1ae0539c4eb62979ae0d8260de2118fb232482'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386439'>#386439</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8608'>D8608</a>
- Be more explicit about Qt5::Widgets dependencies. <a href='https://commits.kde.org/plasma-desktop/1436ce4c23f8e0bc2e4a1efca73a9a436c34331a'>Commit.</a>

### <a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a>

- Enable -DQT_NO_URL_CAST_FROM_STRING and fix compilation. <a href='https://commits.kde.org/plasma-vault/df32c16850e63316fa005521b608ad1f39ef4c97'>Commit.</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Dict Engine: cache available dicts. <a href='https://commits.kde.org/plasma-workspace/1abcb79b5050587b6296bb31dfd999424f5e3f56'>Commit.</a>
- Dict engine: remove two redundant lines. <a href='https://commits.kde.org/plasma-workspace/a9b08ac60431ee34adf064fb3e6da61bc0b10cb7'>Commit.</a>
- Dict engine: remove double quotes around dictionary descriptions. <a href='https://commits.kde.org/plasma-workspace/c72781a4da2401fb2e95ccf63bd134af0d208e3c'>Commit.</a>
- [weather] ion template: remove bogus translation strings. <a href='https://commits.kde.org/plasma-workspace/5e7bd7d0a16855664e79dcc13c73e1d9460e3fbb'>Commit.</a>
- Preserve the order in dbus menu when doing action reusing. <a href='https://commits.kde.org/plasma-workspace/46aaa0e76c7be9ae1039c978458e758ecb424e87'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8585'>D8585</a>
- Be more explicit about Qt5::Widgets dependencies. <a href='https://commits.kde.org/plasma-workspace/ad130ff3bb1fc046713aee6190a9ce3d9c474e9a'>Commit.</a>