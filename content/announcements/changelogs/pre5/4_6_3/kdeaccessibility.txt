------------------------------------------------------------------------
r1227429 | rkcosta | 2011-04-09 13:17:05 +1200 (Sat, 09 Apr 2011) | 4 lines

remove OpenTTS.cmake since opentts is no more and simplify FindSpeechd.cmake.

Backport of r1227422 by whiting.

------------------------------------------------------------------------
r1227430 | rkcosta | 2011-04-09 13:18:01 +1200 (Sat, 09 Apr 2011) | 4 lines

remove checking for opentts and simplify checking for speechd

Backport of r1227423 by whiting.

------------------------------------------------------------------------
r1227431 | rkcosta | 2011-04-09 13:18:49 +1200 (Sat, 09 Apr 2011) | 4 lines

add a message for jovie when trying to build on !X platforms.

Backport of r1227424 and r1227425 respectively by whiting and rkcosta.

------------------------------------------------------------------------
r1227432 | rkcosta | 2011-04-09 13:19:12 +1200 (Sat, 09 Apr 2011) | 6 lines

Link directly to SPEECHD_LIBRARIES instead of SPEECH_LIB.

speechd is the only possible library anyway.

Backport of r1227427 by rkcosta.

------------------------------------------------------------------------
r1227433 | rkcosta | 2011-04-09 13:19:42 +1200 (Sat, 09 Apr 2011) | 6 lines

jovie: Reindent CMakeLists.txt and only build jovie if speechd is found.

In the process, make speechd a required dependency.

Backport of r1227428 by rkcosta.

------------------------------------------------------------------------
r1227437 | rkcosta | 2011-04-09 13:35:55 +1200 (Sat, 09 Apr 2011) | 4 lines

kaccessible: Use macro_log_feature() to show kaccessible may use speechd.

Backport of r1227434 by rkcosta.

------------------------------------------------------------------------
r1227440 | rkcosta | 2011-04-09 13:36:23 +1200 (Sat, 09 Apr 2011) | 4 lines

kaccessible: Link to SPEECHD_LIBRARIES, not speechd.

Backport of r1227435 by rkcosta.

------------------------------------------------------------------------
r1227442 | rkcosta | 2011-04-09 13:56:21 +1200 (Sat, 09 Apr 2011) | 8 lines

kmousetool: Clean up CMakeLists.txt.

 - Always call macro_feature_log().
 - Lowercase all CMake calls.
 - Show an error message on non-X11 systems.

Backport of r1227441 by rkcosta.

------------------------------------------------------------------------
