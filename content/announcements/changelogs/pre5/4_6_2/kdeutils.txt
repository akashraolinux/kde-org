------------------------------------------------------------------------
r1223549 | scripty | 2011-03-04 02:38:01 +1300 (Fri, 04 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1223553 | ingmarvanhassel | 2011-03-04 03:01:25 +1300 (Fri, 04 Mar 2011) | 9 lines

Fix build with libarchive 3.0

libarchive 3.0 no longer defines ARCHIVE_DEFAULT_BYTES_PER_BLOCK,
http://code.google.com/p/libarchive/source/browse/trunk/libarchive/archive.h?r=2455#166

All code examples that libarchive ships, as well as its own functionality
tests hardcode this constant, too.

backport of r1223552
------------------------------------------------------------------------
r1224095 | kossebau | 2011-03-08 10:38:06 +1300 (Tue, 08 Mar 2011) | 1 line

changed: bumped version
------------------------------------------------------------------------
r1224590 | scripty | 2011-03-13 02:39:17 +1300 (Sun, 13 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1225154 | lueck | 2011-03-18 02:51:42 +1300 (Fri, 18 Mar 2011) | 1 line

backport from trunk r1225153: load translation catalog for ark dnd plugin
------------------------------------------------------------------------
r1225567 | teran | 2011-03-22 04:54:40 +1300 (Tue, 22 Mar 2011) | 5 lines

backport of fix for bug #268837

BUG: #268837


------------------------------------------------------------------------
r1225683 | teran | 2011-03-23 11:27:17 +1300 (Wed, 23 Mar 2011) | 3 lines

backport of most recent fix, I had a single signal handler for 2 different cases, could lead to crash


------------------------------------------------------------------------
r1225685 | teran | 2011-03-23 11:35:14 +1300 (Wed, 23 Mar 2011) | 2 lines

backport of the fix for my fix, i missed a spot in my last update

------------------------------------------------------------------------
r1225823 | lueck | 2011-03-24 06:01:30 +1300 (Thu, 24 Mar 2011) | 1 line

backport from trunk r1225822: load translation catalog properly for kwikdisk
------------------------------------------------------------------------
r1225976 | scripty | 2011-03-26 02:24:13 +1300 (Sat, 26 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1226186 | scripty | 2011-03-28 03:11:17 +1300 (Mon, 28 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
