2008-02-28 21:32 +0000 [r780354]  dimsuz

	* branches/KDE/4.0/kdegames/klines/scene.cpp: Backport commit
	  780352 from trunk - show preview closer to scene

2008-02-29 12:35 +0000 [r780544]  lukas

	* branches/KDE/4.0/kdegames/ksquares/src/ksquareswindow.cpp: fix
	  icon

2008-03-01 17:52 +0000 [r780949]  lueck

	* branches/KDE/4.0/kdegames/doc/ksquares/index.docbook,
	  branches/KDE/4.0/kdegames/doc/lskat/index.docbook,
	  branches/KDE/4.0/kdegames/doc/kblackbox/index.docbook,
	  branches/KDE/4.0/kdegames/doc/kspaceduel/index.docbook,
	  branches/KDE/4.0/kdegames/doc/kiriki/index.docbook,
	  branches/KDE/4.0/kdegames/doc/kjumpingcube/index.docbook,
	  branches/KDE/4.0/kdegames/doc/kshisen/index.docbook,
	  branches/KDE/4.0/kdegames/doc/bovo/index.docbook,
	  branches/KDE/4.0/kdegames/doc/kfourinline/index.docbook,
	  branches/KDE/4.0/kdegames/doc/klines/index.docbook,
	  branches/KDE/4.0/kdegames/doc/ksudoku/index.docbook,
	  branches/KDE/4.0/kdegames/doc/kbounce/index.docbook,
	  branches/KDE/4.0/kdegames/doc/ktuberling/index.docbook,
	  branches/KDE/4.0/kdegames/doc/katomic/index.docbook,
	  branches/KDE/4.0/kdegames/doc/konquest/index.docbook: backport
	  from trunk

2008-03-09 11:35 +0000 [r783730]  dimsuz

	* branches/KDE/4.0/kdegames/klines/scene.cpp: Backport commit from
	  trunk - more proper message when no path is found

2008-03-10 23:28 +0000 [r784343]  lunakl

	* branches/KDE/4.0/kdeedu/kwordquiz/src/kwordquiz.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kstarsactions.cpp,
	  branches/KDE/4.0/kdeedu/kig/kig/kig_view.cpp,
	  branches/KDE/4.0/kdesdk/kate/app/katemainwindow.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/QtMainWindow.cpp,
	  branches/KDE/4.0/kdegames/kgoldrunner/src/kgoldrunner.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/chatview.cpp,
	  branches/KDE/4.0/kdegraphics/kolourpaint/mainWindow/kpMainWindow_Settings.cpp,
	  branches/KDE/4.0/kdegraphics/okular/shell/shell.cpp,
	  branches/KDE/4.0/kdegames/ktuberling/toplevel.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/marble_part.cpp,
	  branches/KDE/4.0/kdeedu/kmplot/kmplot/kmplot.cpp: Backport
	  r784333 (remove usage of QWidget::showFullScreen() etc.), use new
	  KToggleFullScreenAction::setFullScreen() helper.

2008-03-11 09:26 +0000 [r784407]  dimsuz

	* branches/KDE/4.0/kdegames/kreversi/kreversiscene.cpp: Backport
	  bugfix from trunk ( r784406 )

2008-03-11 10:21 +0000 [r784431]  dimsuz

	* branches/KDE/4.0/kdegames/klines/scene.cpp,
	  branches/KDE/4.0/kdegames/klines/commondefs.h: Backport a bugfix
	  from trunk (r784428)

2008-03-11 19:33 +0000 [r784580]  mueller

	* branches/KDE/4.0/kdegames/ktuberling/toplevel.cpp: the usual
	  "daily unbreak compilation"

2008-03-13 17:23 +0000 [r785282]  joselb

	* branches/KDE/4.0/kdegames/ksudoku/src/gui/ksudoku.cpp: Fixed
	  Messages BUG: 152840

2008-03-16 12:42 +0000 [r786228]  ducroquet

	* branches/KDE/4.0/kdegames/konquest/mapitems.cc,
	  branches/KDE/4.0/kdegames/konquest/planet.cc,
	  branches/KDE/4.0/kdegames/konquest/gameview.cc,
	  branches/KDE/4.0/kdegames/konquest/planet.h,
	  branches/KDE/4.0/kdegames/konquest/gamelogic.cc: Backport commit
	  786226 to KDE 4.0 (bug 92044)

2008-03-17 19:40 +0000 [r786710]  ducroquet

	* branches/KDE/4.0/kdegames/konquest/player.cc: Backport previous
	  fix

2008-03-27 00:03 +0000 [r790600-790599]  ducroquet

	* branches/KDE/4.0/kdegames/konquest/newgamedlg.cc: Backport
	  temporary fix for the bug that made it mandatory to manually
	  reject the map for the new players settings to be accepted.

	* branches/KDE/4.0/kdegames/konquest/gamelogic.cc: Backport another
	  tiny fix

