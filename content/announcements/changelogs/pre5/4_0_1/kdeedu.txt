2008-01-05 11:40 +0000 [r757564]  rahn

	* branches/KDE/4.0/kdeedu/marble/data/bitmaps/cursor_bl.xpm,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/cursor_cl.xpm,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/cursor_tl.xpm,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/lake.png,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/border_1.png,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/border_2.png,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/cursor_br.xpm,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/cursor_bc.xpm,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/cursor_cr.xpm,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/cursor_tr.xpm,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/cursor_tc.xpm,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/river.png: - Some
	  bitmap fixes

2008-01-05 13:06 +0000 [r757588]  rahn

	* branches/KDE/4.0/kdeedu/marble/data/seacolors.leg,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/bathymetry_-6500.png,
	  branches/KDE/4.0/kdeedu/marble/src/lib/texturepalette.cpp,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/bathymetry_-11000.png,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/bathymetry_-2000.png,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/bathymetry_-4000.png:
	  - Fixes contrast for the oceans

2008-01-05 21:31 +0000 [r757774]  aacid

	* branches/KDE/4.0/kdeedu/kgeography/src/kgeography.cpp: fix
	  getPreferredSize and make kgeography::setMoveActionEnabled
	  actually set the move action enabled using that nice boolean that
	  gets passed

2008-01-06 20:19 +0000 [r758054]  rahn

	* branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.cpp,
	  branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/src/lib/CompassFloatItem.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/geodata/data/GeoDataPoint.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/PlaceMarkLayout.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/CompassFloatItem.h,
	  branches/KDE/4.0/kdeedu/marble/src/kdemain.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleModel.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidgetInputHandler.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/global.h: - Bumping up
	  version number. - Fixed NSWE-Translation in the
	  PlaceMarkInfoDialog. - Fixed up-down direction of toolbar arrow
	  buttons and screen arrow buttons. - Fixed: Free tileloader cache
	  if we switch to plain map - Fixed: In flat projection if you move
	  the map way up or down in total view then the compass rose will
	  show an "S" at the top.

2008-01-07 13:41 +0000 [r758266]  jriddell

	* branches/KDE/4.0/kdeedu/marble/src/icons/wikipedia.png: replace
	  wikipaedia icon, previous one had no copying licence

2008-01-07 19:32 +0000 [r758381]  cartman

	* branches/KDE/4.0/kdeedu/kstars/kstars/indi/drivers/ccd/sbigcam.cpp:
	  backport r758380

2008-01-08 11:00 +0000 [r758573]  mueller

	* branches/KDE/4.0/kdeedu/kstars/kstars/main.cpp: bump version

2008-01-12 10:00 +0000 [r760222]  gladhorn

	* branches/KDE/4.0/kdeedu/parley/src/configure-practice/configurepracticewidget.cpp,
	  branches/KDE/4.0/kdeedu/parley/src/configure-practice/configurepracticewidget.h:
	  Disable the "to language" when a mono lingual practice is
	  selected. Bug: 151354

2008-01-12 15:06 +0000 [r760403]  mlaurent

	* branches/KDE/4.0/kdeedu/kanagram/src/kanagram.cpp: Backport: make
	  help button works

2008-01-12 15:40 +0000 [r760415]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/ksplanetbase.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kssun.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/ksmoon.cpp: Backporting fix
	  to compute angular size of planets

2008-01-12 19:54 +0000 [r760488]  mlaurent

	* branches/KDE/4.0/kdeedu/kbruch/src/mainqtwidget.cpp: Backport:
	  Make help button work

2008-01-12 19:59 +0000 [r760491]  mlaurent

	* branches/KDE/4.0/kdeedu/khangman/src/khangman.cpp: Backport: make
	  help button work

2008-01-12 20:04 +0000 [r760493]  mlaurent

	* branches/KDE/4.0/kdeedu/kiten/app/configuredialog.cpp: Backport:
	  make help button work

2008-01-12 20:15 +0000 [r760495]  mlaurent

	* branches/KDE/4.0/kdeedu/kwordquiz/src/kwordquizprefs.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kstarsactions.cpp,
	  branches/KDE/4.0/kdeedu/klettres/src/klettres.cpp: Backport: make
	  help button works

2008-01-12 20:24 +0000 [r760498]  mlaurent

	* branches/KDE/4.0/kdeedu/parley/src/configure-practice/configurepracticedialog.cpp,
	  branches/KDE/4.0/kdeedu/parley/src/settings/parleyprefs.cpp:
	  Backport: Fix help button

2008-01-12 21:56 +0000 [r760537]  mlaurent

	* branches/KDE/4.0/kdeedu/parley/src/settings/parley.kcfg,
	  branches/KDE/4.0/kdeedu/parley/src/settings/generaloptions.cpp,
	  branches/KDE/4.0/kdeedu/parley/src/settings/parleyprefs.cpp,
	  branches/KDE/4.0/kdeedu/parley/src/settings/generaloptions.h,
	  branches/KDE/4.0/kdeedu/parley/src/settings/generaloptionsbase.ui:
	  Backport: updateButtons doesn't exist =>keep setting dialogbox
	  manages this kcombobox

2008-01-12 23:15 +0000 [r760563]  rahn

	* branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleControlBox.ui,
	  branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/data/vectorsource/lakes.svg,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.h,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleControlBox.cpp,
	  branches/KDE/4.0/kdeedu/marble/data/mwdbii/PLAKE.PNT,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleModel.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/ViewParams.h: *
	  src/lib/ViewParams.h: * src/lib/MarbleWidget.{h, cpp}: *
	  src/lib/MarbleModel.cpp: - Fixed blue tint of space and added
	  setBackgroundTransparency method as part of the fix. - Don't draw
	  the whole screen if the globe doesn't cover it completely. -
	  Fixed Atmosphere not getting drawn when changing projection. *
	  src/lib/MarbleControlBox.cpp: * src/lib/MarbleControlBox.ui: -
	  Getting rid of scrollbars under normal conditions. *
	  data/vectorsource/lakes.svg: * data/mwdbii/PLAKE.PNT: - Fixing
	  lake placement bug as described in easyLINUX (TM) magazine.

2008-01-13 00:57 +0000 [r760585]  rahn

	* branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleModel.h,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarblePlacemarkModel.h,
	  branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.h,
	  branches/KDE/4.0/kdeedu/marble/src/MarbleTest.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/PlaceMarkLayout.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/PlaceMarkLayout.h,
	  branches/KDE/4.0/kdeedu/marble/src/lib/GridMap.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleModel.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarblePlacemarkModel.cpp:
	  * src/lib/MarblePlacemarkModel.{h,cpp}: *
	  src/lib/PlaceMarkLayout.{h,cpp}: - Patch by Claudiu Covaci to get
	  satellite displayed on the far-off hemisphere. - Performance fix
	  to keep speed when zooming in from slowing down. This addresses a
	  regression due to the Model-View-refactorization which made the
	  speed going down by 50%. * src/lib/MarbleWidget.{h,cpp}: *
	  src/lib/MarbleModel.{h,cpp}: - We don't need to reinvent
	  setAutoFillBackground ... - We don't want to draw the atmosphere
	  on close-up zoom - Introduction of direction detection - clean-up
	  * src/lib/GridMap.cpp: Small performance fix *
	  src/MarbleTest.cpp: Fixing default coordinates

2008-01-13 12:37 +0000 [r760765-760763]  cniehaus

	* branches/KDE/4.0/kdeedu/kalzium/src/detailinfodlg.cpp:
	  Backporting the bug to KDE 4.0.1 CCBUG: 155482

	* branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/33.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/51.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/16.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/52.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/7.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/26.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/53.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/27.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/39.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/49.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/79.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/11.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/14.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/50.svg: Sync
	  with trunk. All new icons are now in KDE 4.0.1 as well

2008-01-13 15:46 +0000 [r760875]  ingwa

	* branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleModel.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.h: Implement
	  support for mimimumZoom and maximumZoom in .dgml files

2008-01-14 00:55 +0000 [r761069-761068]  ingwa

	* branches/KDE/4.0/kdeedu/marble/TODO: Cleaned the TODO file to
	  only contain 4.0.x issues

	* branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.h,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleControlBox.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.h: Continue with
	  min and max zoom in .dgml files Almost done now, just need to get
	  the actual min/max zooms from the MapTheme within the Model from
	  the Widget. However, the widget can't get at the real map theme,
	  just its name right now, so we'll have to do some refactoring of
	  the marblemodel.

2008-01-14 01:44 +0000 [r761080]  gladhorn

	* branches/KDE/4.0/kdeedu/parley/src/entry-dialogs/CommonEntryPage.cpp,
	  branches/KDE/4.0/kdeedu/parley/src/entry-dialogs/PhoneticEntryPage.cpp,
	  branches/KDE/4.0/kdeedu/parley/src/entry-dialogs/CommonEntryPage.h,
	  branches/KDE/4.0/kdeedu/parley/src/entry-dialogs/PhoneticEntryPage.h:
	  Use KCharSelect for the phonetics entry page. Much cleaner and
	  less work for me :) Bug:155635 Thanks for the report Florian!

2008-01-14 23:33 +0000 [r761498]  ingwa

	* branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.cpp,
	  branches/KDE/4.0/kdeedu/marble/TODO,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleModel.h,
	  branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.h,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleControlBox.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleModel.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/ControlView.cpp: Finish the
	  feature of supporting minimumZoom and maximumZoom in .dgml files.

2008-01-14 23:43 +0000 [r761501]  ingwa

	* branches/KDE/4.0/kdeedu/marble/ChangeLog: Commit forgotten
	  ChangeLog entry

2008-01-15 10:30 +0000 [r761681]  rahn

	* branches/KDE/4.0/kdeedu/marble/src/lib/TileLoader.h,
	  branches/KDE/4.0/kdeedu/marble/src/lib/TileCreator.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/TileLoader.cpp: As
	  discussed with Dirk ...

2008-01-15 16:59 +0000 [r761913]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/ksplanetbase.cpp:
	  Backporting fix for position angle of planets.

2008-01-16 10:56 +0000 [r762154]  rahn

	* branches/KDE/4.0/kdeedu/marble/data/placemarks/elevplacemarks.cache,
	  branches/KDE/4.0/kdeedu/marble/data/placemarks/elevplacemarks.kml:
	  Moving the Aconcagua to Argentina (had a visit in the gym before
	  accomplishing this task).

2008-01-16 13:43 +0000 [r762184]  sitter

	* branches/KDE/4.0/kdeedu/kiten/CMakeLists.txt,
	  branches/KDE/4.0/kdeedu/kiten/data/pics/CMakeLists.txt: backport
	  r762183 - fix icon installation

2008-01-17 21:46 +0000 [r762793]  gladhorn

	* branches/KDE/4.0/kdeedu/parley/src/entry-dialogs/PhoneticEntryPage.cpp:
	  Make the KCharSelect dialog smaller (no need for so much info
	  there).

2008-01-17 21:56 +0000 [r762801]  gladhorn

	* branches/KDE/4.0/kdeedu/parley/src/language-dialogs/editlanguagedialogpage.cpp:
	  Set the right keyboard layout when opening edit->languages.

2008-01-17 22:14 +0000 [r762809]  gladhorn

	* branches/KDE/4.0/kdeedu/parley/src/language-dialogs/editlanguagedialogpage.cpp:
	  When the locales cannot be found, fall back to using the language
	  name as locale. This probably happens when kdebase is not
	  installed. BUG: 155971

2008-01-18 00:48 +0000 [r762856]  ingwa

	* branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.cpp: Fix a bug
	  that prevented min/max zoom for dgml files to work

2008-01-18 00:53 +0000 [r762857]  rahn

	* branches/KDE/4.0/kdeedu/marble/src/lib/AbstractScanlineTextureMapper.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleModel.h,
	  branches/KDE/4.0/kdeedu/marble/src/lib/FlatScanlineTextureMapper.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/TextureColorizer.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/AbstractScanlineTextureMapper.h,
	  branches/KDE/4.0/kdeedu/marble/src/lib/GlobeScanlineTextureMapper.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.h,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleModel.cpp: *
	  src/lib/TextureColorizer.cpp: - Optimizations. *
	  src/lib/AbstractScanlineTextureMapper.{cpp,h}: *
	  src/lib/MarbleWidget.{cpp,h}: * src/lib/MarbleModel.{cpp,h}: *
	  src/lib/GlobeScanlineTextureMapper.cpp: *
	  src/lib/FlatScanlineTextureMapper.cpp: - First steps towards tile
	  preloading - Removing the headingTowards stuff as I don't need
	  it.

2008-01-18 07:16 +0000 [r762907]  bero

	* branches/KDE/4.0/kdeedu/kstars/kstars/satlib/SatLib.h: Backport
	  rev. 762906: Fix linkage on gcc 4.x with -fvisibility=hidden

2008-01-19 12:43 +0000 [r763439]  cniehaus

	* branches/KDE/4.0/kdeedu/kalzium/src/kalziumutils.cpp: Backport to
	  KDE 4.0.1 CCBUG: 153863

2008-01-19 13:52 +0000 [r763449]  cniehaus

	* branches/KDE/4.0/kdeedu/kalzium/src/exporter.cpp (removed),
	  branches/KDE/4.0/kdeedu/kalzium/src/CMakeLists.txt,
	  branches/KDE/4.0/kdeedu/kalzium/src/exporter.h (removed): *
	  Remove dead code. The code was used nowhere.

2008-01-19 18:32 +0000 [r763551]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/solarsystemsinglecomponent.cpp:
	  Backporting potential fix for bug #154650: Quick fix for planet
	  image size going NaN. Needs testing. This commit doesn't solve
	  the underlying issue, it just firewalls against the NaN.

2008-01-19 18:46 +0000 [r763555]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/customcatalogcomponent.h,
	  branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/customcatalogcomponent.cpp:
	  backporting (potential) fix for Bug #155675: Adding
	  CustomCatalogComponent::update()

2008-01-19 18:54 +0000 [r763563]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/ksplanetbase.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/ksasteroid.h,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kspluto.cpp: Backport fix
	  for bug #132994: Call Pluto an asteroid in the details window

2008-01-19 18:58 +0000 [r763565]  cartman

	* branches/KDE/4.0/kdeedu/cmake/modules/FindBoostPython.cmake: find
	  boost_python in Pardus Linux too

2008-01-19 21:31 +0000 [r763603]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/starcomponent.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/cometscomponent.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/asteroidscomponent.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/ksplanet.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/deepskycomponent.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/tools/planetviewer.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/ksmoon.cpp: Backporting fix
	  for Bug #154195 Replacing "." with KLocale::decimalSymbol() when
	  parsing data files. This fix needs to be tested by someone using
	  a non-US decimalSymbol(). The symptoms are that planets are in
	  the wrong part of the sky, and the Solar system viewer (Ctrl+Y)
	  has "lumpy", irregular orbits. Please email me or comment on the
	  bugs.k.o page if you test!

2008-01-20 20:17 +0000 [r764018]  gladhorn

	* branches/KDE/4.0/kdeedu/parley/src/main.cpp: copyleft

2008-01-20 20:21 +0000 [r764019]  gladhorn

	* branches/KDE/4.0/kdeedu/parley/src/main.cpp,
	  branches/KDE/4.0/kdeedu/parley/src/practice/testentrymanager.cpp,
	  branches/KDE/4.0/kdeedu/parley/src/practice/testentrymanager.h:
	  revert - string freeze

2008-01-20 22:08 +0000 [r764062]  gladhorn

	* branches/KDE/4.0/kdeedu/parley/src/practice/testentrymanager.cpp,
	  branches/KDE/4.0/kdeedu/parley/src/practice/testentrymanager.h:
	  fix compilation

2008-01-21 03:34 +0000 [r764116-764115]  ingwa

	* branches/KDE/4.0/kdeedu/marble/data/maps/earth/citylights/citylights.dgml:
	  Add min and max zoom to the citylights map theme

	* branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.cpp,
	  branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidgetInputHandler.cpp:
	  Fix bug with max/min zoom by moving a check.

2008-01-21 08:51 +0000 [r764168]  rahn

	* branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.cpp: - Some
	  explanations for ingwa

2008-01-23 01:44 +0000 [r765047]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/kstarsinit.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/skymap.cpp: Do not reset
	  the focus position when closing the Options dialog. Backported
	  from trunk.

2008-01-23 14:49 +0000 [r765220-765219]  whiting

	* branches/KDE/4.0/kdeedu/kanagram/src/kanagram.cpp: backport
	  bugfix for BUG:153419

	* branches/KDE/4.0/kdeedu/kanagram/src/kanagram.cpp: fix bug 153419
	  the right way...

2008-01-23 21:29 +0000 [r765340-765337]  piacentini

	* branches/KDE/4.0/kdeedu/kturtle/src/canvas.cpp,
	  branches/KDE/4.0/kdeedu/kturtle/src/canvas.h: Backporting from
	  trunk, go now behaves as in KDE3 (no line drawn)

	* branches/KDE/4.0/kdeedu/kturtle/src/interpreter/translator.cpp:
	  Backporting examples cleanup (no change in strings)

2008-01-24 10:37 +0000 [r765575]  rahn

	* branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.cpp,
	  branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MapScaleFloatItem.cpp:
	  2008-01-24 Torsten Rahn <rahn@kde.org> *
	  src/lib/MarbleWidget.cpp: - Fixing embarassing bug which resulted
	  in a severe slowdown from tile level 9 up. *
	  src/lib/MapScaleFloatItem.cpp: - Some dirty fix for the below-1km
	  issue with the scale bar (this class needs better documentation).
	  * src/lib/MergedLayerPainter.cpp: - Status: FIX->TODO

2008-01-24 13:08 +0000 [r765618]  nielsslot

	* branches/KDE/4.0/kdeedu/kturtle/src/interpreter/definitions.rb,
	  branches/KDE/4.0/kdeedu/kturtle/src/interpreter/parser.cpp:
	  Backported bugfix from trunk, see revision 765612

2008-01-24 18:51 +0000 [r765823]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/kstars.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/colorscheme.h,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kstarsactions.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kstars.kcfg,
	  branches/KDE/4.0/kdeedu/kstars/kstars/colorscheme.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/main.cpp: Backporting
	  persistent color scheme fix from trunk (r765743)

2008-01-24 18:53 +0000 [r765829]  hedlund

	* branches/KDE/4.0/kdeedu/kwordquiz/src/kwordquiz.cpp: Fix layout
	  of overwrite warning. BUG:156329

2008-01-24 18:56 +0000 [r765833]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/kstarsactions.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kstarsinit.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kstarsdata.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kstarsdata.h: Backporting
	  fix for default startup position (r765753)

2008-01-24 19:25 +0000 [r765847]  hedlund

	* branches/KDE/4.0/kdeedu/kwordquiz/src/kwordquiz.cpp: Don't always
	  call Save As BUG:156328

2008-01-24 19:44 +0000 [r765852]  hedlund

	* branches/KDE/4.0/kdeedu/kwordquiz/src/flashviewbase.ui:
	  Flashcards should of course word wrap BUG: 156414

2008-01-26 23:31 +0000 [r766937]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/horizoncomponent.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/skymap.cpp: Backporting
	  cosmetic fixes for projection schemes (r766933). This patch
	  includes a fix for bug #133505 (crash condition in
	  SkyMap::refract())

2008-01-27 10:13 +0000 [r767040]  ingwa

	* branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.h: Start of
	  legend definition in DGML Backported from trunk

2008-01-27 18:31 +0000 [r767290]  rahn

	* branches/KDE/4.0/kdeedu/marble/src/lib/MarbleControlBox.ui,
	  branches/KDE/4.0/kdeedu/marble/ChangeLog: 2008-01-27 Torsten Rahn
	  <rahn@kde.org> * src/lib/MarbleControlBox.ui: set the
	  locationListView's property "uniformItemSizes" "true". This
	  solves the problem of delayed item appearance. Thanks to
	  Trolltech's Alessandro Portale for coming up with this
	  suggestion.

2008-01-27 21:48 +0000 [r767364]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/skymapcomposite.h,
	  branches/KDE/4.0/kdeedu/kstars/kstars/tools/observinglist.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/skymapcomposite.cpp:
	  Backporting fix for bug #147742 to 4.0 branch (allow stars named
	  "star" to be saved to the observing list), and backporting a
	  similar fix for stars with genetive star names (i.e., "sigma
	  orionis"). CCBUG: 147742

2008-01-28 01:08 +0000 [r767413]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/main.cpp: Bump version
	  number in 4.0 branch to 1.3.1 (for KDE 4.0.1)

2008-01-28 06:27 +0000 [r767487]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/tools/modcalcvizequinox.cpp:
	  Backporting patch to fix the display of month names in the
	  equinox/solstice calculator module. Thanks for the patch,
	  Mederic!

2008-01-28 21:07 +0000 [r767822]  aacid

	* branches/KDE/4.0/kdeedu/kgeography/src/main.cpp: increase version

2008-01-28 22:18 +0000 [r767866]  rahn

	* branches/KDE/4.0/kdeedu/marble/data/bitmaps/bathymetry_-50.png
	  (removed), branches/KDE/4.0/kdeedu/marble/src/lib/MarbleModel.h,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/topography_0.png
	  (removed),
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleLegendBrowser.h,
	  branches/KDE/4.0/kdeedu/marble/data/maps/earth/srtm/srtm.dgml,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/topography_200.png
	  (removed), branches/KDE/4.0/kdeedu/marble/data/legend.html,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/bathymetry_-6500.png
	  (removed),
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/topography_500.png
	  (removed), branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.cpp,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/bathymetry_-11000.png
	  (removed),
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/bathymetry_-200.png
	  (removed), branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.h,
	  branches/KDE/4.0/kdeedu/marble/data/CMakeLists.txt,
	  branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/topography_1000.png
	  (removed),
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/topography_2000.png
	  (removed),
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleControlBox.cpp,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/topography_5000.png
	  (removed),
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/topography_50.png
	  (removed),
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/topography_7000.png
	  (removed),
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/topography_3500.png
	  (removed),
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/bathymetry_-2000.png
	  (removed),
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleModel.cpp,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/bathymetry_0.png
	  (removed),
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/bathymetry_-4000.png
	  (removed),
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleLegendBrowser.cpp:
	  This fixes the wrong Legend entry being shown for all map themes
	  except for Atlas View: 2008-01-28 Torsten Rahn <rahn@kde.org> *
	  src/lib/MapTheme.{cpp,h}: * src/lib/MarbleLegendBrowser.{cpp,h}:
	  * data/maps/earth/citylights/citylights.dgml: *
	  data/maps/earth/bluemarble/bluemarble.dgml: - Allowing a
	  completely custom legend.html per theme - changing the relative
	  position of the pixmap path in the dgml file to allow access to
	  the data/bitmaps directory. 2008-01-28 Torsten Rahn
	  <rahn@kde.org> * src/lib/MapTheme.{cpp,h}: *
	  src/lib/MarbleLegendBrowser.{cpp,h}: *
	  src/lib/MarbleModel.{cpp,h}: * src/lib/MapTheme.h: *
	  data/maps/earth/srtm/srtm.dgml: * data/legend.html: *
	  data/CMakeLists.txt:

2008-01-29 05:21 +0000 [r767941]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/opssolarsystem.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kstars.kcfg: Backporting
	  maximum asteroid magnitude limit change (from 10 to 30)

2008-01-29 13:08 +0000 [r768119]  rahn

	* branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/lake.png,
	  branches/KDE/4.0/kdeedu/marble/src/lib/FlatScanlineTextureMapper.cpp,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/border_1.png,
	  branches/KDE/4.0/kdeedu/marble/data/maps/earth/srtm/srtm.dgml,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/border_2.png,
	  branches/KDE/4.0/kdeedu/marble/data/legend.html,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.cpp,
	  branches/KDE/4.0/kdeedu/marble/data/bitmaps/river.png,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.h,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleLegendBrowser.cpp:
	  2008-01-29 Torsten Rahn <rahn@kde.org> *
	  src/lib/MapTheme.{cpp,h}: * src/lib/MarbleLegendBrowser.cpp: *
	  src/lib/FlatScanlineTextureMapper.cpp: *
	  data/bitmaps/border_1.png: * data/bitmaps/border_2.png: *
	  data/bitmaps/river.png: * data/bitmaps/lake.png: *
	  data/maps/earth/citylights/citylights.dgml: *
	  data/maps/earth/plain/plain.dgml: *
	  data/maps/earth/srtm/srtm.dgml: *
	  data/maps/earth/bluemarble/bluemarble.dgml: * data/legend.html: -
	  Implementing "name", "checkable" and "spacing" attribute for
	  LegendSections. - Making existing dgml files use it. - Changing
	  the marker to "...:all" to allow for custom placement of each
	  section in the legend.html.

2008-01-29 14:56 +0000 [r768149-768147]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/skymapcomposite.h,
	  branches/KDE/4.0/kdeedu/kstars/kstars/opscatalog.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/skymapcomposite.cpp:
	  Backporting custom catalog initialization fix

	* branches/KDE/4.0/kdeedu/kstars/kstars/opscatalog.cpp: Remove
	  debugs

2008-01-29 18:37 +0000 [r768309]  nielsslot

	* branches/KDE/4.0/kdeedu/kturtle/src/interpreter/executer.cpp,
	  branches/KDE/4.0/kdeedu/kturtle/src/interpreter/definitions.rb:
	  Also fix bug 156437 in 4.0 branch (backported from revision
	  768306)

2008-01-29 20:03 +0000 [r768333]  piacentini

	* branches/KDE/4.0/kdeedu/kturtle/src/main.cpp: Bumping patch
	  version number to track the bug fixes better

2008-01-29 20:31 +0000 [r768341]  rahn

	* branches/KDE/4.0/kdeedu/marble/data/maps/earth/citylights/citylights-preview.png,
	  branches/KDE/4.0/kdeedu/marble/data/maps/earth/plain/plain-preview.png,
	  branches/KDE/4.0/kdeedu/marble/src/lib/AbstractScanlineTextureMapper.cpp,
	  branches/KDE/4.0/kdeedu/marble/data/CMakeLists.txt,
	  branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/data/maps/earth/srtm/srtm-preview.png,
	  branches/KDE/4.0/kdeedu/marble/data/svg/application-x-marble.svg
	  (added), branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.cpp,
	  branches/KDE/4.0/kdeedu/marble/data/maps/earth/bluemarble/bluemarble-preview.png,
	  branches/KDE/4.0/kdeedu/marble/data/svg/application-x-marble-gray.png
	  (added),
	  branches/KDE/4.0/kdeedu/marble/data/svg/application-x-marble.png
	  (added), branches/KDE/4.0/kdeedu/marble/src/lib/MapTheme.h:
	  2008-01-29 Torsten Rahn <rahn@kde.org> *
	  src/lib/MapTheme.{cpp,h}: *
	  src/lib/AbstractScanlineTextureMapper.cpp: - Fixing a few
	  memleaks and uninitialized values - Adding a bit bling bling.

2008-01-29 22:00 +0000 [r768369]  rahn

	* branches/KDE/4.0/kdeedu/marble/src/lib/FlatScanlineTextureMapper.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/kdemain.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/marble_part.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/global.h: 2008-01-29
	  Torsten Rahn <rahn@kde.org> * src/kdemain.cpp: *
	  src/lib/global.h: * src/marble_part.cpp: - Marbleheads of the
	  World, Rejoice: we are at version 0.5.1 now! *
	  src/lib/FlatScanlineTextureMapper.cpp: - Fixing valgrind's
	  finding of "invalid write (4 bytes)".

2008-01-29 22:16 +0000 [r768373]  rahn

	* branches/KDE/4.0/kdeedu/marble/Messages.sh: getting the .dgml
	  file fully translated ...

2008-01-29 22:59 +0000 [r768388]  rahn

	* branches/KDE/4.0/kdeedu/marble/Messages.sh: - Messages.sh

2008-01-30 00:14 +0000 [r768411]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/data/ngcic.dat: Backporting
	  patch from Akarsh to fix the object type of NGC 1975 and NGC
	  1977.

2008-01-30 00:56 +0000 [r768420]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/deepskyobject.cpp:
	  Backporting Akarsh's patch to tighten up label placement of
	  elongated objects.

2008-01-30 08:19 +0000 [r768484]  harris

	* branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/starcomponent.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/cometscomponent.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/asteroidscomponent.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/ksplanet.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/skycomponents/deepskycomponent.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/tools/planetviewer.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/ksmoon.cpp: Backporting
	  final fix for bug #154195, just in time for 4,0,1 (sorry, I
	  thought I had done it already). CCBUG: 154195

2008-01-30 12:49 +0000 [r768599]  cniehaus

	* branches/KDE/4.0/kdeedu/libkdeedu/libscience/data/elements.xml:
	  These four date where missing because of a Perlscript bug in
	  BlueObelsik. This is a backport from trunk

2008-01-30 14:47 +0000 [r768728]  sengels

	* branches/KDE/4.0/kdeedu/kpercentage/kpercentage/icons/ox128-apps-kpercentage.png,
	  branches/KDE/4.0/kdeedu/kpercentage/kpercentage/icons/ox48-apps-kpercentage.png,
	  branches/KDE/4.0/kdeedu/kpercentage/kpercentage/icons/ox32-apps-kpercentage.png,
	  branches/KDE/4.0/kdeedu/kpercentage/kpercentage/icons/ox16-apps-kpercentage.png,
	  branches/KDE/4.0/kdeedu/kpercentage/kpercentage/icons/ox64-apps-kpercentage.png:
	  although this is not the optimum, please leave the grayscale
	  optimization out for application icons

2008-01-30 20:01 +0000 [r768863]  rahn

	* branches/KDE/4.0/kdeedu/marble/Messages.sh: - Forgot the
	  "heading" tag ...

