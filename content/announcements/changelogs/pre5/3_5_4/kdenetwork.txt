2006-06-12 13:48 +0000 [r550636]  mueller

	* branches/KDE/3.5/kdenetwork/kppp/accounts.cpp,
	  branches/KDE/3.5/kdenetwork/lanbrowsing/lisa/netscanner.cpp,
	  branches/KDE/3.5/kdenetwork/kppp/main.cpp,
	  branches/KDE/3.5/kdenetwork/lanbrowsing/lisa/main.cpp: add
	  various return value checks

2006-06-19 08:48 +0000 [r552828]  binner

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ircnetworks.xml:
	  backport #548119

2006-06-24 08:05 +0000 [r554523]  hasso

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ircnetworks.xml:
	  The only working Freenode server in the list was irc.kde.org :(.
	  Can't test whether SSL works here though. CCMAIL: kde-et@linux.ee

2006-07-01 07:45 +0000 [r556681]  binner

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp:
	  remove debug code printing uninitiliased variable

2006-07-09 12:00 +0000 [r560149]  lueck

	* branches/KDE/3.5/kdenetwork/doc/kpf/index.docbook: documentation
	  backport from trunk CCMAIL:kde-doc-english

2006-07-11 18:00 +0000 [r561016]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/oscartypes.h:
	  Backport fix for bug 130630 "ICQ server thinks the client you are
	  using is too old" CCBUG: 130630

2006-07-16 10:18 +0000 [r562938]  kling

	* branches/KDE/3.5/kdenetwork/krdc/krdc.cpp: Don't delete the
	  "Advanced" popup menu without intentions of recreating it. BUG:
	  116913

2006-07-16 14:20 +0000 [r563036]  kling

	* branches/KDE/3.5/kdenetwork/krdc/krdc.cpp,
	  branches/KDE/3.5/kdenetwork/krdc/main.cpp: Make "always show
	  local cursor" and "view only" permanently configurable via
	  krdcrc. Since we're in a GUI/message freeze, I can't add any GUI
	  to these options until KDE4. The options and their defaults are:
	  viewOnly=false alwaysShowLocalCursor=false BUG: 96174 BUG: 108620

2006-07-16 14:32 +0000 [r563039]  kling

	* branches/KDE/3.5/kdenetwork/krdc/krdc.cpp: Handle "smb:/" as well
	  as "smb://" URLs, since both are valid (or at least possible.)
	  BUG: 103932

2006-07-18 20:16 +0000 [r563911]  kling

	* branches/KDE/3.5/kdenetwork/krdc/rdp/rdphostpref.h,
	  branches/KDE/3.5/kdenetwork/krdc/rdp/rdpprefs.ui.h,
	  branches/KDE/3.5/kdenetwork/krdc/preferencesdialog.cpp: Remember
	  new default settings for RDP color depth and keyboard layout.
	  BUG: 123772

2006-07-18 22:15 +0000 [r563947]  kling

	* branches/KDE/3.5/kdenetwork/krdc/hostprofiles.ui.h,
	  branches/KDE/3.5/kdenetwork/krdc/vnc/kvncview.cpp,
	  branches/KDE/3.5/kdenetwork/krdc/rdp/krdpview.h,
	  branches/KDE/3.5/kdenetwork/krdc/preferencesdialog.h,
	  branches/KDE/3.5/kdenetwork/krdc/vnc/kvncview.h,
	  branches/KDE/3.5/kdenetwork/krdc/hostprofiles.ui,
	  branches/KDE/3.5/kdenetwork/krdc/rdp/krdpview.cpp,
	  branches/KDE/3.5/kdenetwork/krdc/preferencesdialog.cpp: Made the
	  host profiles editable (by double-clicking them in the
	  preferences dialog.) BUG: 116932

2006-07-19 16:26 +0000 [r564234]  kling

	* branches/KDE/3.5/kdenetwork/krfb/kcm_krfb/configurationwidget.ui:
	  Allow listening on all TCP ports between 1024 and 65535. BUG:
	  77626

2006-07-23 14:02 +0000 [r565474]  coolo

	* branches/KDE/3.5/kdenetwork/kdenetwork.lsm: preparing KDE 3.5.4

