learn-more:
  other: "Learn More"
announcements:
  other: "Announcements"
releases:
  other: "Releases"
products:
  other: "Products"
develop:
  other: "Develop"
rss-feed:
  other: "RSS Feed"
kde-patrons:
  other: "KDE Patrons"
donate-to-kde:
  other: "Donate to KDE"
why-donate:
  other: "Why Donate?"
donate-via-paypal:
  other: "Donate via PayPal"
development-and-communication:
  other: "Development and communication of KDE software"
destinations:
  other: "Destinations"
timeline:
  other: "KDE Timeline"
international-websites:
  other: "International Websites"
download-kde:
  other: "Download KDE Software"
coc:
  other: "Code of Conduct"
ressources:
  other: "Resources"
news-and-press:
  other: "News & Press"
kde-applications:
  other: "KDE applications"
api-doc:
  other: "API Documentation"
screenshots:
  other: "Screenshots"
press-contact:
  other: "Press Contacts"
visit-kde-metastore:
  other: "Visit the KDE MetaStore"
show-love-store:
  other: "Show your love for KDE! Purchase books, mugs, apparel, and more to support KDE."
click-store:
  other: "Browse"
other-donate:
  other: "Other ways to donate"
return-kde:
  other: "Return to kde.org"
about-kde:
  other: "About KDE" 
about-kde-text:
  other: "KDE is an international technology team that creates free and open source software for desktop and portable computing. Among KDE's products are a modern desktop system for Linux and UNIX platforms, comprehensive office productivity and groupware suites and hundreds of software titles in many categories including Internet and web applications, multimedia, entertainment, educational, graphics and software development. KDE software is translated into more than 60 languages and is built with ease of use and modern accessibility principles in mind. KDE's full-featured applications run natively on Linux, BSD, Solaris, Windows and Mac OS X."
trademark-notices:
  other: "Trademark Notices."
kde-trademark:
  other: "KDE<sup>&#174;</sup> and the K Desktop Environment<sup>&#174;</sup> logo are registered trademarks of KDE e.V." 
linux-trademark:
  other: "Linux is a registered trademark of Linus Torvalds."
unix-trademark:
  other: "UNIX is a registered trademark of The Open Group in the United States and other countries."
other-trademarks:
  other: "All other trademarks and copyrights referred to in this announcement are the property of their respective owners."
info-email:
  other: "For more information send us an email:"
live-images:
  other: "Live Images"
live-images-text:
  other: "The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma."
download-plasma:
  other: "Download live images with Plasma 5"
docker-plasma:
  other: "Download Docker images with Plasma 5"
package-downloads:
  other: "Package Downloads"
package-downloads-text:
  other: "Distributions have created, or are in the process of creating, packages listed on our wiki page."
package-wiki:
  other: "Package download wiki page"
source-download:
  other: "Source Downloads"
source-download-text:
  other: "You can install Plasma 5 directly from source."
community-instructions:
  other: "Community instructions to compile it"
source-info-page:
  other: "Source Info Page"
feedback:
  other: "Feedback"
give-feedback:
  other: "You can give us feedback and get updates on our social media channels:"
post-facebook:
  other: "Post on Facebook"
share-twitter:
  other: "Share on Twitter"
share-diaspora:
  other: "Share on Diaspora"
share-reddit:
  other: "Share on Reddit"
share-youtube:
  other: "Share on YouTube"
share-mastodon:
  other: "Share on Mastodon"
share-linkedin:
  other: "Share on LinkedIn"
share-peertube:
  other: "Share on Peertube"
share-vk:
  other: "Share on VK"
share-instagram:
  other: "Share on Instagram"
report-bug-plasma:
  other: "You can provide feedback direct to the developers via the [#Plasma IRC channel](irc://#plasma@freenode.net), [Plasma-devel mailing list](https://mail.kde.org/mailman/listinfo/plasma-devel) or report issues via [Bugzilla](https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided). If you like what the team is doing, please let them know!"
feedback-apprciated:
  other: "Supporting KDE"
support:
  other: "Support"
whatiskde:
  other: "KDE is a [Free Software](https://www.gnu.org/philosophy/free-sw.html) community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the [Supporting KDE page](/community/donations/) for further information or become a KDE e.V. supporting member through our [Join the Game](https://relate.kde.org/civicrm/contribute/transact?id=5) initiative."
plasma-5-20:
  other: Plasma 5.20
plasma-5-20-slogan:
  other: New and improved inside and out
plasma-5-20-description:
  other: A massive release, containing improvements to dozens of components, widgets, and the desktop behavior in general.
release-announcements:
  other: Release Announcements
kde-framework:
  other: "KDE Frameworks {{ .Params.version }}"
binary-package:
  other: "Installing binary packages"
binary-package-text:
  other: 'On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks. [Get KDE Software on Your Linux Distro wiki page]({{ .Get "wiki" }})'
sources:
  other: "Compiling from sources"
sources-text:
  other: 'The complete source code for KDE Frameworks {{ .Get "version" }} may be [freely downloaded](https://download.kde.org/stable/frameworks/{{ .Get "version2" }}/). Instructions on compiling and installing KDE Frameworks {{ .Get "version" }} are available from the [KDE Frameworks {{ .Get "version" }} Info Page](/info/kde-frameworks-{{ .Get "version" }}).'
sources-instructions:
  other: 'Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to [use kdesrc-build]({{ .Get "url" }}). Frameworks {{ .Get "version" }} requires Qt {{ .Get "qtversion" }}.'
sources-details:
  other: 'A detailed listing of all Frameworks and other third party Qt libraries is at [inqlude.org]({{ .Get "inqlude" }}), the curated archive of Qt libraries.  A complete list with API documentation is on [api.kde.org]({{ .Get "apiurl" }}).'
get-involed-framework:
  other: 'Those interested in following and contributing to the development of Frameworks can check out the [git repositories]({{ .Get "frameworkurl" }}) and follow the discussions on the [KDE Frameworks Development mailing list]({{ .Get "frameworkmail" }}). Policies and the current state of the project and plans are available at the [Frameworks wiki](https://community.kde.org/Frameworks). Real-time discussions take place on the [#kde-devel IRC channel on freenode.net](irc://#kde-devel@freenode.net).'
plasma:
  other: "Plasma"
plasma-desc:
  other: "The next generation desktop for Linux"
plasma-install:
  other: Install on your computer
plasma-discover:
  other: Discover Plasma
plasma-buy:
  other: Buy a computer with Plasma
kde-apps:
  other: KDE Applications
kde-powerful:
  other: Powerful, multi-platform and for all
kde-app-description:
  other: Use KDE software to surf the web, keep in touch with colleagues, friends and family, manage your files, enjoy music and videos; and get creative and productive at work. The KDE community develops and maintains more than <strong>200</strong> applications which run on any Linux desktop, and often other platforms too.
see-all-apps:
  other: See all applications
krita:
  other: Krita
krita-desc:
  other: Get creative and draw beautiful artwork with Krita. A professional grade painting application.
krita-screen:
  other: Krita screenshot
kdenlive:
  other: Kdenlive
kdenlive-desc:
  other: Kdenlive allows you to edit your videos and add special effects and transitions.
kdenlive-screen:
  other: Kdenlive Screenshot
kontact:
  other: Kontact
kontact-desc:
  other: Handle all your emails, calendars and contacts within a single window with Kontact.
kontact-screen:
  other: Kontact Screenshot
kdevelop:
  other: Kdevelop
kdevelop-desc:
  other: KDevelop is a cross-platform IDE for C, C++, Python, QML/JavaScript and PHP
kdevelop-screen:
  other: KDevelop Screenshot
gcompris:
  other: GCompris
gcompris-desc:
  other: GCompris is a high quality educational software suite, including a large number of activities for children aged 2 to 10.
gcompris-screen:
  other: GCompris Screenshot
hardware:
  other: Hardware
buy-hardware:
  other: Buy a computer with Plasma preinstalled
pinebook-pro:
  other: "Pinebook Pro"
pinebook-pro-desc:
  other: The Pinebook Pro is an affordable ARM powered laptop. It is modular and hackable in a way that only an Open Source project can be.
slimbook:
  other: KDE Slimbook 
slimbook-pro:
  other: The Slimbook is a shiny, sleek and good looking laptop that can do any task thanks to its powerful intel core-series processor.
kfocus:
  other: KFocus
kfocus-desc:
  other: The Kubuntu Focus laptop is a high-powered, workflow-focused laptop which ships with Kubuntu installed.
other-hardware:
  other: Other hardware manufacturers are selling computers with Plasma.
view-all-announcements:
  other: View all announcements
kde-community:
  other: The KDE Community
kde-community-desc:
  other: An international team developing and distributing Open Source software.
kde-community-desc2:
  other: Our community has developed a wide variety of applications for communication, work, education and entertainment. We have a strong focus on finding innovative solutions to old and new problems, creating a vibrant, open atmosphere for experimentation.
group-photo-lakademy:
  other: Group photo of LaKademy 2018
plasma-sprint:
  other: Plasma Sprint
get-involved:
  other: Get Involved
kde-goals:
  other: KDE Goals
kdenlive-sprint:
  other: Kdenlive Sprint
paris:
  other: Paris, France
Salvador:
  other: Salvador, Brazil
berlin:
  other: Berlin, Germany
delhi:
  other: Delhi, India
full-changelog:
  other: 'You can find the full list of changes [here](/announcements/changelogs/applications/{{ .Get "version" }}).'
spread-the-word:
  other: Spread the Word
non-technical-success:
  other: "Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the KDE Applications release. Report bugs. Encourage others to join the KDE Community. Or [support the nonprofit organization behind the KDE community](https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=9)"
spread-social:
  other: Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, and twitter. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with 'KDE'. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for this KDE Applications release.
install-applications:
  other: Installing KDE Applications Binary Packages
packaes:
  other: Packages
packages-desc:
  other: Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.
package-location:
  other: Package Locations
package-location-desc:
  other: "For a current list of available binary packages of which the KDE Project has been informed, please visit the [Community Wiki](https://community.kde.org/KDE_Applications/Binary_Packages)."
compile-app:
  other: Compiling KDE Applications
compile-app-desc:
  other: 'The complete source code for KDE Applications may be [freely downloaded](https://download.kde.org/stable/applications/{{ .Get "version" }}/src). Instructions on compiling and installing are available from the [KDE Applications {{ .Get "version" }} Info Page](/info/applications-{{ .Get "version" }}).'
translations:
  other: "Languages"
florianopolis:
  other: "Florianopolis, Brazil"
barcelona:
  other: "Barcelona"
kde-connect-sprint:
  other: "KDE Connect Sprint"
gsoc-students:
  other: "Google Summer of Code Students"
gsoc-students-photo:
  other: "KDE GSoC students group photo"
Select-your-language:
  other: Select your language
see-all-hardware:
  other: See all hardware
read-more:
  other: Read Full Story
whatiskde-button:
  other: What is KDE?
amount:
  other: Amount
home:
  other: Home
code-of-conduct:
  other: Code of conduct
privacy-policy:
  other: Privacy Policy
privacy-policy-apps:
  other: Applications Privacy Policy
stuff:
  other: Miscellaneous Stuff
qt-doc:
  other: Qt Documentation
inqlude-doc:
  other: Inqlude Documentation
shareMatrix:
  other: Share on Matrix
kde-frameworks:
  other: KDE Frameworks
plasma-mobile:
  other: Plasma Mobile
wikitolearn:
  other: WikiToLearn
techbase:
  other: Techbase Wiki
kde-neon:
  other: KDE Neon
kde-news:
  other: KDE.news
planet-kde:
  other: Planet KDE
community-wiki:
  other: Community Wiki
userbase:
  other: UserBase Wiki
kde-store:
  other: KDE Store
kde-ev:
  other: KDE e.V.
kde-free-qt:
  other: KDE Free Qt Foundation
maintenance-address:
  other: Maintained by <a href="mailto:kde-www@kde.org">KDE Webmasters</a> (public mailing list).
generating-commit:
  other: Generated from {{ .Get "hash" }}.
trademark-with-link:
  other: KDE<sup>®</sup> and [the K Desktop Environment<sup>®</sup> logo](https://kde.org/media/images/trademark_kde_gear_black_logo.png) are registered trademarks of [KDE e.V.](https://ev.kde.org/ "Homepage of the KDE non-profit Organization")
legal:
  other: Legal
patron-alt-canonical:
  other: 'Canonical'
patron-alt-google:
  other: 'Google'
patron-alt-suse:
  other: 'SUSE'
patron-alt-qc:
  other: 'The Qt Company'
patron-alt-bs:
  other: 'Blue System'
patron-alt-ehc:
  other: 'enioka Haute Couture'
donation-too-small:
  other: "Your donation is smaller than 2€. This means that most of your donation\nwill end up in processing fees. Do you want to continue?"
next-slide:
  other: "Next slide"
prev-slide:
  other: "Previous slide"
skip-to-content:
  other: "Skip to content"
view-changelog:
  other: 'View full changelog'
date-format:
  other: '%A, %d %B %Y'
Monday:
  other: Monday
Tuesday:
  other: Tuesday
Wednesday:
  other: Wednesday
Thursday:
  other: Thursday
Friday:
  other: Friday
Saturday:
  other: Saturday
Sunday:
  other: Sunday
January:
  other: January
February:
  other: February
March:
  other: March
April:
  other: April
May:
  other: May
June:
  other: June
July:
  other: July
August:
  other: August
September:
  other: September
October:
  other: October
November:
  other: November
December:
  other: December
